<cfcomponent accessors="true">

	<cfproperty name="mandrillAPI" />
	<cfproperty name="splunkService" />

	<cffunction name="init">
		<cfset variables.MongoCollectionSubscribers = 'subscribers' />
		<cfset variables.MongoCollectionContacts 	= 'contacts' />
		<cfset variables.MongoSubscribers 			= application.mongo.getDBCollection( variables.MongoCollectionSubscribers ) />
		<cfset variables.MongoContacts 				= application.mongo.getDBCollection( variables.MongoCollectionContacts ) />
	</cffunction>

	<cffunction name="addContactInfo" access="public" returntype="struct">
		<cfargument name="name" type="string" required="true" />
		<cfargument name="email" type="string" required="true" />
		<cfargument name="comments" type="string" required="true" />

		<cfset returnStruct = structNew() />
		<cfset returnStruct.errors = 0 />
		<cfset returnStruct.errorMessage = 'true' />

		<cftry>
			<cfset dateNow = dateFormat(now(), "YYYY-MM-DD") & " " & TimeFormat(now(), "HH:mm:ss") />
			<cfset newContact = {
				"name": ARGUMENTS.name,
				"email": ARGUMENTS.email,
				"comment": ARGUMENTS.comments,
				"insert_date": dateNow
			} />
			<cfset variables.MongoContacts.save(newContact) />
			<cfset copy = "
				We have a new contact! Here's the info: <br/><br/>
				NAME: #ARGUMENTS.name# <br/>
				EMAIL: <a href='mailto:#ARGUMENTS.email#' style='color:##ef5721'>#ARGUMENTS.email#</a>, <br/>
				MESSAGE: <br/>
					#ARGUMENTS.comments# <br/>
			" />
			<cfsavecontent
			    variable = "emailHTML">
			    <cfinclude template="/emails/template/inner-email.cfm" />
			</cfsavecontent>

			<cfset arrContacts = #ListToArray(application.env.SETTINGS.EMAILLIST, ',')# />
			<cfset emailContacts = arrayNew(1) />
			<cfloop from="1" to="#arrayLen(arrContacts)#" index="i">
				<cfset contactInfo = structNew() />
				<cfset contactInfo['name'] = '' />
				<cfset contactInfo['email'] = #arrContacts[i]#/>
				<cfset arrayAppend(emailContacts, contactInfo) />
			</cfloop>

			<!--- Send Email Thru Mandrill --->
			<cfset sendMail = getMandrillAPI().send(
				<!--- Key --->
				"#application.env.settings.keyMandrillAPI#",
				<!--- HTML --->
				"#emailHTML#",
				<!--- Text --->
				"New Contact Request Received, view it at www.flukyfactory.com/admin",
				<!--- Subject --->
				"[Fluky] New Contact Request: #ARGUMENTS.name#",
				<!--- From Email --->
				"info@flukyfactory.com",
				<!--- From Name --->
				"Fluky Factory Site",
				<!--- Contacts Info --->
				#emailContacts#,
				<!--- Track Opens --->
				true,
				<!--- Track Clicks --->
				true
			) />

			<!---Log contact data to splunkStorm--->
			<cfset logData = structNew() />
			<cfset logData.event = 'date="#dateFormat(NOW(), "MM-DD-YYY")#", time="#timeFormat(NOW(), 'hh:mm"ss')#", subject="New Contact Received", data="name:#ARGUMENTS.name#,email:#ARGUMENTS.email#,comments:#ARGUMENTS.comments#"' />
			<cfset logData.sourceType = 'info' />
			<cfset logContact = getSplunkService().logData(argumentCollection = logData) />

			<cfcatch type="any">
				<cfset returnStruct.errors = 1 />
				<cfset returnStruct.errorMessage = '#cfcatch.message#, #cfcatch.detail#' />
				<!---Log error data to splunkStorm--->
				<cfset logData = structNew() />
				<cfset logData.event = 'date="#dateFormat(NOW(), "MM-DD-YYY")#", time="#timeFormat(NOW(), 'hh:mm"ss')#", subject="ContactsService:addContactInfo()", data="#cfcatch.message# - #cfcatch.detail#' />
				<cfset logData.sourceType = 'error' />
				<cfset logContact = getSplunkService().logData(argumentCollection = logData) />
			</cfcatch>
		</cftry>
		<cfreturn returnStruct />
	</cffunction>

	<cffunction name="getContacts" access="public" returntype="array">
		<cfargument name="id" require="false" type="string" default="0" />

		<cfset arrayContacts = ArrayNew(1) />
		<cftry>
			<cfif ARGUMENTS.id NEQ "0">
				<cfset tempStruct = variables.MongoContacts.findById(ARGUMENTS.id,variables.MongoCollectionContacts) />
				<cfset ArrayAppend(arrayContacts, tempStruct) />
			<cfelse>
				<cfset arrayContacts = variables.MongoContacts.query().find().asArray() />
			</cfif>
			<cfcatch type="any">
				<!---Log error data to splunkStorm--->
				<cfset logData = structNew() />
				<cfset logData.event = 'date="#dateFormat(NOW(), "MM-DD-YYY")#", time="#timeFormat(NOW(), 'hh:mm"ss')#", subject="ContactsService:getContacts()", data="#cfcatch.message# - #cfcatch.detail#' />
				<cfset logData.sourceType = 'error' />
				<cfset logContact = getSplunkService().logData(argumentCollection = logData) />
			</cfcatch>
		</cftry>

		<cfreturn arrayContacts />
	</cffunction>

	<cffunction name="deleteContact" access="public" returntype="struct">
		<cfargument name="id" type="string" required="true" />

		<cfset structResult = structNew() />
		<cfset structResult.code = 1 />
		<cfset structResult.message = "" />

		<cftry>
			<cfset contact = variables.MongoContacts.findById(ARGUMENTS.id,variables.MongoCollectionContacts) />
			<cfset variables.MongoContacts.remove(contact) />
		<cfcatch type="any">
			<cfset structResult.code = 0 />
			<cfset structResult.message = cfcatch.message />
			<!---Log error data to splunkStorm--->
			<cfset logData = structNew() />
			<cfset logData.event = 'date="#dateFormat(NOW(), "MM-DD-YYY")#", time="#timeFormat(NOW(), 'hh:mm"ss')#", subject="ContactsService:deleteContact()", data="#cfcatch.message# - #cfcatch.detail#' />
			<cfset logData.sourceType = 'error' />
			<cfset logContact = getSplunkService().logData(argumentCollection = logData) />
		</cfcatch>
		</cftry>

		<cfreturn structResult />
	</cffunction>

	<cffunction name="getSubscriber" access="public" returntype="array">
		<cfargument name="id" require="false" type="string" default="0" />

		<cfset arraySubscribers = ArrayNew(1) />
		<cftry>
			<cfif ARGUMENTS.id NEQ "0">
				<cfset tempStruct = variables.MongoSubscribers.findById(ARGUMENTS.id,variables.MongoCollectionSubscribers) />
				<cfset ArrayAppend(arraySubscribers, tempStruct) />
			<cfelse>
				<cfset arraySubscribers = variables.MongoSubscribers.query().find().asArray() />
			</cfif>
			<cfcatch type="any">
				<!---Log error data to splunkStorm--->
				<cfset logData = structNew() />
				<cfset logData.event = 'date="#dateFormat(NOW(), "MM-DD-YYY")#", time="#timeFormat(NOW(), 'hh:mm"ss')#", subject="ContactsService:getSubscriber()", data="#cfcatch.message# - #cfcatch.detail#' />
				<cfset logData.sourceType = 'error' />
				<cfset logContact = getSplunkService().logData(argumentCollection = logData) />
			</cfcatch>
		</cftry>

		<cfreturn arraySubscribers />
	</cffunction>

	<cffunction name="addSubscriber" access="public" returntype="struct" hint="Add a subscriber to the DB">
		<cfargument name="email" require="true" type="string" />
		<cfargument name="lang" require="true" type="string" />

		<cfset structResult = structNew() />
		<cfset structResult.code = 1 />
		<cfset structResult.message = "true" />

		<cftry>
			<cfset findSubscriberCount = variables.MongoSubscribers.query().$eq("email", "#ARGUMENTS.email#").count() />
			<cfif findSubscriberCount EQ 0 >
				<cfset dateNow = dateFormat(now(), "YYYY-MM-DD") & " " & TimeFormat(now(), "HH:mm:ss") />
				<cfset newSubscriber = {
					"email": "#ARGUMENTS.email#",
					"language": "#ARGUMENTS.lang#",
					"active": 1,
					"source": "Fluky Site",
					"insert_date": dateNow
				} />
				<cfset variables.MongoSubscribers.save(newSubscriber) />
			<cfelse>
				<cfset structResult.message = "duplicate" />
			</cfif>

			<!---Log subscribe data to splunkStorm--->
			<cfset logData = structNew() />
			<cfset logData.event = 'date="#dateFormat(NOW(), "MM-DD-YYY")#", time="#timeFormat(NOW(), 'hh:mm"ss')#", subject="New Subscription Email Received", data="email:#ARGUMENTS.email#,lang:#ARGUMENTS.lang#"' />
			<cfset logData.sourceType = 'info' />
			<cfset logContact = getSplunkService().logData(argumentCollection = logData) />

		<cfcatch type="any">
			<cfset structResult.code = 0 />
			<cfset structResult.message = cfcatch.message />
			<!---Log error data to splunkStorm--->
			<cfset logData = structNew() />
			<cfset logData.event = 'date="#dateFormat(NOW(), "MM-DD-YYY")#", time="#timeFormat(NOW(), 'hh:mm"ss')#", subject="ContactsService:addSubscriber()", data="#cfcatch.message# - #cfcatch.detail#' />
			<cfset logData.sourceType = 'error' />
			<cfset logContact = getSplunkService().logData(argumentCollection = logData) />
		</cfcatch>
		</cftry>

		<cfreturn structResult />
	</cffunction>

	<cffunction name="removeSubscriber" access="public" returntype="struct" hint="Change active to inactive to unsubscribe">
		<cfargument name="id" require="true" type="string" />

		<cfset structResult = structNew() />
		<cfset structResult.code = 1 />
		<cfset structResult.message = "true" />

		<cftry>
			<cfset subscriber = variables.MongoSubscribers.findById(ARGUMENTS.id,variables.MongoCollectionSubscribers) />
			<cfset subscriber.active = 0 />
			<cfset variables.MongoSubscribers.save(subscriber) />
		<cfcatch type="any">
			<cfset structResult.code = 0 />
			<cfset structResult.message = cfcatch.message />
			<!---Log error data to splunkStorm--->
			<cfset logData = structNew() />
			<cfset logData.event = 'date="#dateFormat(NOW(), "MM-DD-YYY")#", time="#timeFormat(NOW(), 'hh:mm"ss')#", subject="ContactsService:removeSubscriber()", data="#cfcatch.message# - #cfcatch.detail#' />
			<cfset logData.sourceType = 'error' />
			<cfset logContact = getSplunkService().logData(argumentCollection = logData) />
		</cfcatch>
		</cftry>

		<cfreturn structResult />
	</cffunction>

</cfcomponent>