<cfcomponent name="utilities" accessors="true">

	<!--- Dependency Injection --->

	<!---
		Method: _redirect("[section.item]","[list of variables]")
		This is a custom redirect(), since the fw/1 doesnt work well with subsystems
		and SES.

		This method needs to have app.defaultSubSystem declared.(for a reason, fw/1
		changes the default subsytem after app start).
	--->
		<cffunction name="_redirect" output="false" access="public" returnType="void">
			<cfargument name="path" required="true" />
			<cfargument name="rcData" required="false" default="" />
			<cfset cosmeticAction = fw.getFullyQualifiedAction( arguments.path ) />
			<cfset isHomeAction = cosmeticAction is fw.getFullyQualifiedAction( fw.getConfig().home ) />
			<cfset isDefaultItem = fw.getItem( cosmeticAction ) is fw.getConfig().defaultItem />

			<!--- Store every var in session scope to retrive it in the setup request --->
			<cfloop list="#rcData#" index="i">
				<cfset session.rc[i] = request.context[i] />
			</cfloop>

			<!--- Creating the SES for the section.item with its subsystem if needed --->
			<cfif fw.usingSubsystems() and fw.getSubsystem( cosmeticAction ) is fw.getConfig().defaultSubSystem>
				<cfset cosmeticAction = fw.getSectionAndItem( cosmeticAction ) />
			</cfif>

			<!--- Creating the SES for the section.item with its subsystem if needed --->
			<cfif isHomeAction>
				<cfset basePath = "/" />
			<cfelseif isDefaultItem>
				<cfset basePath = "/" & Replace( ReReplace( listFirst( cosmeticAction, '.' ), '\:|\.', '/', 'all' ), 'default', '' ) />
			<cfelse>
				<cfset basePath = "/" & Replace( ReReplace( cosmeticAction, '\:|\.', '/', 'all' ), 'default', '' ) />
			</cfif>

			<!--- This is the new redirect --->
			<cflocation url="#basePath#" addToken="no" />
		</cffunction>

	<!---
		Method: _buildURL("[section.item]", "[querystring]")
		This is a custom buildURL(), since the fw/1 doesnt work well with subsystems
		and SES.

		*This is a View only function...
	--->
		<cffunction name="_buildURL" output="false" access="public" returnType="string">
			<cfargument name="path" required="true" />
			<cfargument name="querystring" required="false" default="" />
			<cfset cosmeticQuerystring = "" />
			<cfset cosmeticAction = getFullyQualifiedAction( arguments.path ) />
			<cfset isHomeAction = cosmeticAction is getFullyQualifiedAction( variables.framework.home ) />
			<cfset isDefaultItem = getItem( cosmeticAction ) is variables.framework.defaultItem />

			<!--- Pasing the querystring to a kinda SES --->
			<cfset argsArray = listToArray(querystring, "&") />
			<cfloop array="#argsArray#" index="i">
				<cfset cosmeticQuerystring = cosmeticQuerystring & "/" & listLast(i, '=') />
			</cfloop>
			<cfset cosmeticQuerystring = Replace(cosmeticQuerystring,' ','-' ,'all') />

			<cfif usingSubsystems() and getSubsystem( cosmeticAction ) is variables.framework.defaultSubSystem>
				<cfset cosmeticAction = getSectionAndItem( cosmeticAction ) />
			</cfif>

			<!--- Creating the SES for the section.item with its subsystem if needed --->
			<cfif isHomeAction>
				<cfset basePath = "/" />
			<cfelseif isDefaultItem>
				<cfset basePath = "/" & Replace( ReReplace( listFirst( cosmeticAction, '.' ), '\:|\.', '/', 'all' ), 'default', '' ) />
			<cfelse>
				<cfset basePath = "/" & Replace( ReReplace( cosmeticAction, '\:|\.', '/', 'all' ), 'default', '' ) />
			</cfif>

			<!--- Return the path with the querystring as SES --->
			<cfreturn basePath & cosmeticQuerystring />
		</cffunction>

</cfcomponent>