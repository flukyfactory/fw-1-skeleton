<!---
	Filename: subsystem.cfm
	Date: Jul/2013
	Developers:
		Lester Barahona lbarahona@flukyfactory.com
	Description:
		Send email notifications through Mandrill API.
	NOTE:
		For the moment, just the most basic parameters of the send option are implemented.
--->

<cfcomponent>

	<cffunction name="send" action="public" returntype="struct">
		<cfargument name="key" type="string" required="true" />
		<cfargument name="html" type="string" required="true" />
		<cfargument name="text" type="string" required="false" default="" />
		<cfargument name="subject" type="string" required="true" />
		<cfargument name="fromEmail" type="string" required="true" />
		<cfargument name="fromName" type="string" required="false" default="" />
		<cfargument name="receivers" type="array" required="true" />
		<cfargument name="trackOpens" type="boolean" required="false" default="false" />
		<cfargument name="trackClicks" type="boolean" required="false" default="false" />

		<cfset returnStruct = structNew() />
		<cfset returnStruct.status = 0 />
		<cfset returnStruct.message = '' />

		<cftry>

			<cfset structMailInfo = structNew() />
			<cfset structMailInfo['key'] = ARGUMENTS.key />
			<cfset structMailInfo['message'] = structNew() />
			<cfset structMailInfo['message']['html'] = ARGUMENTS.html />
			<cfif len(ARGUMENTS.text)>
				<cfset structMailInfo['message']['text'] = ARGUMENTS.text />
			</cfif>
			<cfset structMailInfo['message']['subject'] = ARGUMENTS.subject />
			<cfset structMailInfo['message']['from_email'] = ARGUMENTS.fromEmail />
			<cfif len(ARGUMENTS.fromName)>
				<cfset structMailInfo['message']['from_name'] = ARGUMENTS.fromName />
			</cfif>
			<cfset structMailInfo['message']['to'] = ARGUMENTS.receivers />
			<cfset structMailInfo['message']['track_opens'] = ARGUMENTS.trackOpens />
			<cfset structMailInfo['message']['track_clicks'] = ARGUMENTS.trackClicks />
			<cfset structMailInfo['message']['inline_css'] = true />

			<!---Call Mandrill API to send message--->
			<cfhttp url="https://mandrillapp.com/api/1.0/messages/send.json" method="post">
     			<cfhttpparam type="body" value="#serializeJSON(structMailInfo)#" />
			</cfhttp>

			<cfset returnInfo = deserializeJSON(cfhttp.fileContent) />

			<cfif findNocase('200', cfhttp.statusCode)>
				<cfset returnInfo = returnInfo[1] />
				<cfset returnStruct.status = returnInfo.status />
			<cfelse>
				<cfset returnStruct.status = returnInfo.status />
				<cfset returnStruct.message = returnInfo.message />
			</cfif>


		<cfcatch type="any">
			<cfset returnStruct.status = 'error' />
			<cfset returnStruct.message = '#cfcatch.message#, #cfcatch.detail#' />
		</cfcatch>
		</cftry>

		<cfreturn returnStruct />
	</cffunction>

</cfcomponent>