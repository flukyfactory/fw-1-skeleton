<!---
	Filename: splunkService.cfc
	Date: Nov/2013
	Developers:
		Lester Barahona lbarahona@flukyfactory.com
	Description:
		Log errors and statistics through Splunk Storm API.
	NOTE:
		Parameters to access Splunk Storm API are configured in environment.cfm.

	Example of usage:
	<cfset rc.event = 'date="#dateFormat(NOW(), "MM-DD-YYY")#", time="#timeFormat(NOW(), 'hh:mm"ss')#", text1="text example2", description="just a test description longer"' />
	<cfset rc.sourceType = 'error' /> or warning, or info, or something meaningful
	<cfset variables.fw.service('splunkService.logData', 'result') />
--->

<cfcomponent accessors="true">

	<cfset variables.host = CGI.SERVER_NAME />

	<cffunction name="logData" access="public" returntype="void">
		<cfargument name="sourceType" type="string" required="true" default="default" />
		<cfargument name="event" type="string" required="true" />

		<cfhttp url="#application.env.settings.splunk.APIURL#?index=#application.env.settings.splunk.projectID#&sourcetype=#arguments.sourceType#&host=#variables.host#&source=#CGI.REMOTE_ADDR#" method="post"
		username="x" password="#application.env.settings.splunk.token#" charset="utf-8">

			<cfhttpparam type="header" name="Content-Type" value="text/plain" />
			<cfhttpparam type="body" value="#arguments.event#" />

		</cfhttp>
	</cffunction>

</cfcomponent>