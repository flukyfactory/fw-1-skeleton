<!---
	setupRequest.cfm
	Common setupRequest settings for all fw/1
--->

<!--- This is used to pass RC scope vars thru the custom redirect function --->
<cfif structKeyExists(session, "rc")>
	<cfset structAppend(rc, session.rc) />
	<cfset structClear(session.rc) />
</cfif>