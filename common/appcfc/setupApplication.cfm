<!---
	setupApplication.cfm
	Common setupApplication settings for all fw/1
--->

	<!--- Application Variables --->
		<cfset application.env.name = getEnvironment() />
		<cfset application.env.settings = variables.framework.environments[getEnvironment()] />
		<cfset application.logAppStart = createObject('component','common.services.splunkService') />
		<cfset application.settings = getSubsystemConfig(getSubSystem()) />
		<cfset application.defaultSubSystem = (!structKeyExists(variables.framework, 'defaultSubSystem'))? variables.framework.defaultSubSystem: "public" />
		<cfset setBeanFactory(this.bf) />

	<!--- Mongo Setup --->
		<cfif isDefined("application.mongo")>
			<cfset application.mongo.close() />
			<cfset StructDelete(application,'mongo') />
			<!---Log MongoDB connection closed splunkStorm--->
			<cfset logData = structNew() />
			<cfset logData.event = 'date="#dateFormat(NOW(), "MM-DD-YYY")#", time="#timeFormat(NOW(), 'hh:mm"ss')#", subject="MongoDB closed connection"' />
			<cfset logData.sourceType = 'info' />
			<cfset application.logAppStart.logData(argumentCollection = logData) />
			<cflog text="cfmongodb connection closed" type="info" file="mongodb">
		</cfif>
		<cfset mongoConfig = createObject('component','common.cfmongo.MongoConfig')
								.init(hosts = application.env.settings.mongo.hosts, dbName=application.env.settings.mongo.dsn) />
		<cfset application.mongo = createObject('component','common.cfmongo.MongoClient').init(mongoConfig) />

	<!---Log MongoDB connection opened to splunkStorm--->
		<cfset logData = structNew() />
		<cfset logData.event = 'date="#dateFormat(NOW(), "MM-DD-YYY")#", time="#timeFormat(NOW(), 'hh:mm"ss')#", subject="MongoDB connection opened"' />
		<cfset logData.sourceType = 'info' />
		<cfset application.logAppStart.logData(argumentCollection = logData) />
		<cflog text="cfmongodb connection opened" type="info" file="mongodb">

	<!--- Mappings by subSystem --->
		<cfset this.mappings['/config'] 		= expandPath("/core/config/") />
		<cfset this.mappings['/model'] 			= expandPath("/#getSubsystem()#/model/") />
		<cfset this.mappings['/views'] 			= expandPath("/#getSubsystem()#/views/") />
		<cfset this.mappings['/services'] 		= expandPath("/#getSubsystem()#/services/") />
		<cfset this.mappings['/controllers'] 	= expandPath("/#getSubsystem()#/controllers/") />
		<cfset this.mappings['/layouts'] 		= expandPath("/#getSubsystem()#/layouts/") />
		<cfset this.mappings['/assets'] 		= expandPath("/#getSubsystem()#/assets/") />

	<!--- Activity Log --->
		<cflog text="#Application.applicationname# with subsystem #getSubsystem()# started | #CGI.REMOTE_ADDR#" type="info" file="fw1">