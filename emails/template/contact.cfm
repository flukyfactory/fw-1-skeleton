<cfparam name="copy" default="" />
<!-- ***************************************************
********************************************************

HOW TO USE: Use these code examples as a guideline for formatting your HTML email. You may want to create your own template based on these snippets or just pick and choose the ones that fix your specific rendering issue(s). There are two main areas in the template: 1. The header (head) area of the document. You will find global styles, where indicated, to move inline. 2. The body section contains more specific fixes and guidance to use where needed in your design.

DO NOT COPY OVER COMMENTS AND INSTRUCTIONS WITH THE CODE to your message or risk spam box banishment :).

It is important to note that sometimes the styles in the header area should not be or don't need to be brought inline. Those instances will be marked accordingly in the comments.

********************************************************
**************************************************** -->

<!-- Using the xHTML doctype is a good practice when sending HTML email. While not the only doctype you can use, it seems to have the least inconsistencies. For more information on which one may work best for you, check out the resources below.

UPDATED: Now using xHTML strict based on the fact that gmail and hotmail uses it.  Find out more about that, and another great boilerplate, here: http://www.emailology.org/#1

More info/Reference on doctypes in email:
Campaign Monitor - http://www.campaignmonitor.com/blog/post/3317/correct-doctype-to-use-in-html-email/
Email on Acid - http://www.emailonacid.com/blog/details/C18/doctype_-_the_black_sheep_of_html_email_design
-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>Flukyfactory</title>
	<style type="text/css">

		/***********
		Originally based on The MailChimp Reset from Fabio Carneiro, MailChimp User Experience Design
		More info and templates on Github: https://github.com/mailchimp/Email-Blueprints
		http://www.mailchimp.com &amp; http://www.fabio-carneiro.com

		INLINE: Yes.
		***********/
		/* Client-specific Styles */
		#outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
		body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
		/* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
		.ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing.  More on that: http://www.emailonacid.com/forum/viewthread/43/ */
		#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
		/* End reset */

		/* Some sensible defaults for images
		1. "-ms-interpolation-mode: bicubic" works to help ie properly resize images in IE. (if you are resizing them using the width and height attributes)
		2. "border:none" removes border when linking images.
		3. Updated the common Gmail/Hotmail image display fix: Gmail and Hotmail unwantedly adds in an extra space below images when using non IE browsers. You may not always want all of your images to be block elements. Apply the "image_fix" class to any image you need to fix.

		Bring inline: Yes.
		*/
		img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}
		a img {border:none;}
		.image_fix {display:block;}

		/** Yahoo paragraph fix: removes the proper spacing or the paragraph (p) tag. To correct we set the top/bottom margin to 1em in the head of the document. Simple fix with little effect on other styling. NOTE: It is also common to use two breaks instead of the paragraph tag but I think this way is cleaner and more semantic. NOTE: This example recommends 1em. More info on setting web defaults: http://www.w3.org/TR/CSS21/sample.html or http://meiert.com/en/blog/20070922/user-agent-style-sheets/

		Bring inline: Yes.
		**/
		p {margin: 1em 0;}

		/** Hotmail header color reset: Hotmail replaces your header color styles with a green color on H2, H3, H4, H5, and H6 tags. In this example, the color is reset to black for a non-linked header, blue for a linked header, red for an active header (limited support), and purple for a visited header (limited support).  Replace with your choice of color. The !important is really what is overriding Hotmail's styling. Hotmail also sets the H1 and H2 tags to the same size.

		Bring inline: Yes.
		**/
		h1, h2, h3, h4, h5, h6 {color: white !important;}

		h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}

		h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
			color: red !important; /* Preferably not the same color as the normal header link color.  There is limited support for psuedo classes in email clients, this was added just for good measure. */
		 }

		h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
			color: purple !important; /* Preferably not the same color as the normal header link color. There is limited support for psuedo classes in email clients, this was added just for good measure. */
		}

		/** Outlook 07, 10 Padding issue: These "newer" versions of Outlook add some padding around table cells potentially throwing off your perfectly pixeled table.  The issue can cause added space and also throw off borders completely.  Use this fix in your header or inline to safely fix your table woes.

		More info: http://www.ianhoar.com/2008/04/29/outlook-2007-borders-and-1px-padding-on-table-cells/
		http://www.campaignmonitor.com/blog/post/3392/1px-borders-padding-on-table-cells-in-outlook-07/

		H/T @edmelly

		Bring inline: No.
		**/
		table td {border-collapse: collapse;}

		/** Remove spacing around Outlook 07, 10 tables

		More info : http://www.campaignmonitor.com/blog/post/3694/removing-spacing-from-around-tables-in-outlook-2007-and-2010/

		Bring inline: Yes
		**/
		table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }

		/* Styling your links has become much simpler with the new Yahoo.  In fact, it falls in line with the main credo of styling in email, bring your styles inline.  Your link colors will be uniform across clients when brought inline.

		Bring inline: Yes. */
		a {color: orange;}

		/* Or to go the gold star route...
		a:link { color: orange; }
		a:visited { color: blue; }
		a:hover { color: green; }
		*/

		/***************************************************
		****************************************************
		MOBILE TARGETING

		Use @media queries with care.  You should not bring these styles inline -- so it's recommended to apply them AFTER you bring the other stlying inline.

		Note: test carefully with Yahoo.
		Note 2: Don't bring anything below this line inline.
		****************************************************
		***************************************************/

		/* NOTE: To properly use @media queries and play nice with yahoo mail, use attribute selectors in place of class, id declarations.
		table[class=classname]
		Read more: http://www.campaignmonitor.com/blog/post/3457/media-query-issues-in-yahoo-mail-mobile-email/
		*/
		@media only screen and (max-device-width: 480px) {

			/* A nice and clean way to target phone numbers you want clickable and avoid a mobile phone from linking other numbers that look like, but are not phone numbers.  Use these two blocks of code to "unstyle" any numbers that may be linked.  The second block gives you a class to apply with a span tag to the numbers you would like linked and styled.

			Inspired by Campaign Monitor's article on using phone numbers in email: http://www.campaignmonitor.com/blog/post/3571/using-phone-numbers-in-html-email/.

			Step 1 (Step 2: line 224)
			*/
			a[href^="tel"], a[href^="sms"] {
						text-decoration: none;
						color: black; /* or whatever your want */
						pointer-events: none;
						cursor: default;
					}

			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
						text-decoration: default;
						color: orange !important; /* or whatever your want */
						pointer-events: auto;
						cursor: default;
					}
		}

		/* More Specific Targeting */

		@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
			/* You guessed it, ipad (tablets, smaller screens, etc) */

			/* Step 1a: Repeating for the iPad */
			a[href^="tel"], a[href^="sms"] {
						text-decoration: none;
						color: blue; /* or whatever your want */
						pointer-events: none;
						cursor: default;
					}

			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
						text-decoration: default;
						color: orange !important;
						pointer-events: auto;
						cursor: default;
					}
		}

		@media only screen and (-webkit-min-device-pixel-ratio: 2) {
			/* Put your iPhone 4g styles in here */
		}

		/* Following Android targeting from:
		http://developer.android.com/guide/webapps/targeting.html
		http://pugetworks.com/2011/04/css-media-queries-for-targeting-different-mobile-devices/  */
		@media only screen and (-webkit-device-pixel-ratio:.75){
			/* Put CSS for low density (ldpi) Android layouts in here */
		}
		@media only screen and (-webkit-device-pixel-ratio:1){
			/* Put CSS for medium density (mdpi) Android layouts in here */
		}
		@media only screen and (-webkit-device-pixel-ratio:1.5){
			/* Put CSS for high density (hdpi) Android layouts in here */
		}
		/* end Android targeting */
	</style>

	<!-- Targeting Windows Mobile -->
	<!--[if IEMobile 7]>
	<style type="text/css">

	</style>
	<![endif]-->

	<!-- ***********************************************
	****************************************************
	END MOBILE TARGETING
	****************************************************
	************************************************ -->

	<!--[if gte mso 9]>
	<style>
		/* Target Outlook 2007 and 2010 */
	</style>
	<![endif]-->
</head>
<body style="background-color:#282828;">
	<!-- Wrapper/Container Table: Use a wrapper table to control the width and the background color consistently of your email. Use this approach instead of setting attributes on the body tag. -->
	<table cellpadding="0" cellspacing="0" border="0" id="backgroundTable" style="background-color:#282828;" width="100%">
		<tr>
			<td align="center">

			<!-- Tables are the most common way to format your email consistently. Set your table widths inside cells and in most cases reset cellpadding, cellspacing, and border to zero. Use nested tables as a way to space effectively in your message. -->
				<table cellpadding="0" cellspacing="0" border="0" width="670"  style="background-color:#34495e;">
					<tr><!-- HEADER -->
						<td width="100%" height="120" valign="top" style="background-color:#333333;">
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
								<tr>
									<td width="420" style="padding:40px 0 0 50px;" valign="top">
										<img src="http://www.flukyfactory.com/emails/template/target-images/flukyfactory-logo.jpg" alt="" width="218" height="46" border="0" />
									</td>
									<td width="250" valign="top" style="padding:42px 0 0 0;">

										<table cellpadding="0" cellspacing="0" border="0" width="100%">
											<tr>
												<td height="50" width="50" valign="top" align="left">
													<a href="http://www.facebook.com/flukyfactory" style="width:47px; height:47px; text-decoration:none;">
														<img src="http://www.flukyfactory.com/emails/template/target-images/flukyfactory-facebook.jpg" alt="" width="47" height="47" border="0" />
													</a>
												</td>
												<td height="50" width="50" valign="top" align="left">
													<a href="http://www.twitter.com/flukyfactory" style="width:47px; height:47px; text-decoration:none;">
														<img src="http://www.flukyfactory.com/emails/template/target-images/flukyfactory-twitter.jpg" alt="" width="47" height="47" border="0" />
													</a>
												</td>
												<td height="50" width="50" valign="top" align="left">
													<a href="http://www.linkedin.com/company/fluky-factory?goback=%2Efcs_GLHD_fluky+factory_false_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2&trk=top_nav_home" style="width:47px; height:47px; text-decoration:none;">
														<img src="http://www.flukyfactory.com/emails/template/target-images/flukyfactory-linkedin.jpg" alt="" width="47" height="47" border="0" />
													</a>
												</td>
												<td height="50" width="50" valign="top" align="left">
													<a href="https://plus.google.com/u/0/b/105935772343268673057/105935772343268673057/posts" style="width:47px; height:47px; text-decoration:none;">
														<img src="http://www.flukyfactory.com/emails/template/target-images/flukyfactory-google+.jpg" alt="" width="47" height="47" border="0" />
													</a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr><!-- COPY -->
						<td width="100%" valign="top" style="background-color:#34495e; padding:43px 0;" align="center">
							<table cellpadding="0" cellspacing="0" border="0" width="560">
								<tr>
									<td valign="top" style="font-size:20px; line-height:30px;">
										<div style="color:#fff; font-family: Verdana, sans-serif; margin:0 0 48px 0; ">
											<p style="font-size:20px; line-height:30px;">
												<cfoutput>#copy#</cfoutput>
											</p>
										</div>
									</td>
								</tr>
							</table>
							<img src="http://www.flukyfactory.com/emails/template/target-images/flukyfactory-divider.gif" width="604" height="1" alt="" />
						</td>
					</tr>
					<tr><!-- NOW US -->
						<td width="100%" height="220" valign="top" style="background-color:#34495e;" align="center">
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
								<tr>
									<td width="265" style="padding: 0 13px 0 30px;" valign="top" align="right">
										<h1 style="font-size:32px; line-height:44px; color:#fff; margin:0 0 7px 0; padding:0; font-family: Verdana, sans-serif; text-align:right; font-weight:normal; width:100%;">&iquest;Est&aacute;s preparado para<br>un sitio M&oacute;vil?</h1>
										<table cellpadding="0" cellspacing="0" border="0" width="250" height="45">
											<tr>
												<td valign="top" width="100%" height="45" style="background:#f15925; color:#fff; text-align:center; text-transform:uppercase; font-family: Verdana, sans-serif; font-size:11px; text-decoration:none;">
													<a href="http://www.flukyfactory.com" style="width:100%; height:45px; text-decoration:none; color:#fff; font-size:11px; display:block; padding:0;">
														<div style="margin:0; padding: 15px 0 0 0; font-size:11px; line-height:12px;">
															HAZ CL&iacute;CK AQU&iacute; para cONOCERNOS
														</div>
													</a>
												</td>
											</tr>
										</table>
										<!--- <a href="http://www.flukyfactory.com" style="width:235px; height:31px; background:#f15925; color:#fff; text-align:center; display:block; text-transform:uppercase; padding-top:15px; font-family: Verdana, sans-serif; float:right; margin:8px 0 0 0; font-size:11px; text-decoration:none;" >
											HAZ CL&iacute;CK AQU&iacute; para cONOCERNOS
										</a> --->
										<div style="clear:both;"></div>
									</td>
									<td width="175" valign="top">
										<img src="http://www.flukyfactory.com/emails/template/target-images/flukyfactory-mobile-devices.jpg" alt="" width="168" height="139" border="0" />
									</td>
								</tr>
								<tr>
									<td height="44" ></td>
								</tr>
							</table>
							<img src="http://www.flukyfactory.com/emails/template/target-images/flukyfactory-divider.gif" width="604" height="1" alt="" />
						</td>
					</tr>
					<tr><!-- SERVICES -->
						<td align="center" height="235" valign="top" style="background-color:#34495e;">
							<table cellpadding="0" cellspacing="0" border="0" width="490" style="margin:17px 0;">
								<tr>
									<td width="100%" valign="top">
										<h2 style="font-size:23px; font-family: Verdana, sans-serif; margin: 0; padding:0; font-weight:normal; color:#ffffff;">Contamos con los siguientes servicios:</h2>
									</td>
								</tr>
								<tr>
									<td align="center" valign="top" style="padding: 30px 0 0px 0;">
										<table cellpadding="0" cellspacing="0" border="0" width="490">
											<tr>
												<td width="55%" style="text-align:left;" valign="top" height="35">
													<table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%">
														<tr>
															<td valign="top" width="40" height="100%">
																<img src="http://www.flukyfactory.com/emails/template/target-images/flukyfactory-email.gif" alt="" width="22" height="14" border="0" />
															</td>
															<td valign="top" height="100%" style="font-size:16px; color:#fff; font-family: Verdana, sans-serif; display:inline-block; margin:0;padding:0 0 0 0px; ">
																E-Marketing
															</td>
														</tr>
													</table>
												</td>
												<td width="45%" style="text-align:left;" valign="top" height="35">
													<table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%">
														<tr>
															<td valign="top" width="35" height="100%">
																<img src="http://www.flukyfactory.com/emails/template/target-images/flukyfactory-pencil.gif" alt="" width="18" height="15" border="0" />
															</td>
															<td valign="top" height="100%" style="font-size:16px; color:#fff; font-family: Verdana, sans-serif; display:inline-block; margin:0;padding:0 0 0 0px; ">
																Dise&ntilde;o Gr&aacute;fico
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td width="55%" style="text-align:left;" valign="top" height="35">
													<table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%">
														<tr>
															<td valign="top" width="40" height="100%">
																<img src="http://www.flukyfactory.com/emails/template/target-images/flukyfactory-keyboard.gif" alt="" width="24" height="15" border="0" />
															</td>
															<td valign="top" height="100%" style="font-size:16px; color:#fff; font-family: Verdana, sans-serif; display:inline-block; margin:0;padding:0 0 0 0px; ">
																Front-End
															</td>
														</tr>
													</table>
												</td>
												<td width="45%" style="text-align:left;" valign="top" height="35">
													<table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%">
														<tr>
															<td valign="top" width="35" height="100%">
																<img src="http://www.flukyfactory.com/emails/template/target-images/flukyfactory-chat.gif" alt="" width="18" height="16" border="0" />
															</td>
															<td valign="top" height="100%" style="font-size:16px; color:#fff; font-family: Verdana, sans-serif; display:inline-block; margin:0;padding:0 0 0 0px; ">
																Redes
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td width="55%" style="text-align:left;" valign="top" height="35">
													<table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%">
														<tr>
															<td valign="top" width="40" height="100%">
																<img src="http://www.flukyfactory.com/emails/template/target-images/flukyfactory-db.gif" alt="" width="13" height="17" border="0" />
															</td>
															<td valign="top" height="100%" style="font-size:16px; color:#fff; font-family: Verdana, sans-serif; display:inline-block; margin:0;padding:0 0 0 0px; ">
																Soporte a Servidores
															</td>
														</tr>
													</table>
												</td>
												<td width="45%" style="text-align:left;" valign="top" height="35">
													<table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%">
														<tr>
															<td valign="top" width="35" height="100%">
																<img src="http://www.flukyfactory.com/emails/template/target-images/flukyfactory-world.gif" alt="" width="18" height="15" border="0" />
															</td>
															<td valign="top" height="100%" style="font-size:16px; color:#fff; font-family: Verdana, sans-serif; display:inline-block; margin:0;padding:0 0 0 0px; ">
																Hosting y Dominio
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<img src="http://www.flukyfactory.com/emails/template/target-images/flukyfactory-divider.gif" width="604" height="1" alt="" />
						</td>
					</tr>
					<tr><!-- WORLD OF TECHNOLOGY -->
						<td align="center" height="200" valign="bottom" style="background-color:#34495e;">
							<table cellpadding="0" cellspacing="0" border="0" width="600">
								<tr>
									<td width="300" valign="top">
										<p style="margin:0; padding:67px 0 0 0; color:#688291; font-family: Verdana, sans-serif; text-align:right; font-size:25px; line-height:28px;">Te ubicamos en el<br>mundo de la tecnolog&iacute;a</p>
									</td>
									<td width="300" valign="bottom">
										<img src="http://www.flukyfactory.com/emails/template/target-images/flukyfactory-world.jpg" alt="" width="291" height="181" border="0" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr><!-- FOOTER -->
						<td valign="top" width="100%" height="35" style="background-color:#fff; padding:15px 0 0 0;">
							<p style="margin: 0; padding: 0; color:#757575; font-size:9px; font-family: Verdana, sans-serif; line-height:11px; text-align:center; text-transform:uppercase;">
								<a style="text-decoration:none; color:#ef5721;" href="http://www.flukyfactory.com">www.FLUKYFACTORY.COM</a> - <a style="text-decoration:none; color:#ef5721;" href="mailto:sales@flukyfactory.com">sales@flukyfactory.com</a>
							</p>
							<p style="margin: 0; padding: 0; color:#757575; font-size:9px; font-family: Verdana, sans-serif; line-height:11px; text-align:center; text-transform:uppercase;">
								TODOS LOS DERECHOS RESERVADO. <span style="color:#ef5721;">HECHO EN HONDURAS.</span>
							</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<!-- End of wrapper table -->
</body>
</html>