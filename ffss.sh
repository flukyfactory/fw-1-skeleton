#!/bin/sh
#
#	Initialization Script
#	Written by: Luis Matute & Douglas Barahona
#
#	For RHEL 6 x64
# 	Basic Service Setup for Git, MySQL, MongoDB, Railo and NGINX
#

##Initialice Variables##
TODAY=`date '+%d-%m-%Y'`
SCRIPT_NAME="FFSS"
VERSION="2.0"
TEMP_DIR="/data/tmp"
WEBROOT_DIR="/webroot"
INSTALL_MYSQL=0
INSTALL_MONGODB=0
DEFAULT_PASSWORD="F@ct0ry123"
DOMAIN="flukyfactory.com"

##Display command help function##
exit_help() {
    more <<EOF
    MANUAL

    NAME
        ffss - server setup as needed for Fluky Factory

    SYNOPSIS
        ffss.sh [-v] [-h] [-t [=<path>]] [-w [=<path>]] [-m] [-o] [-p [=<value>]] -d [=<value>]

    DESCRIPTION
        This script will help create production environment for servers of Fluky Factory. This script will update the server OS, install git, NGINX, MySQL, MongoDB, Railo, configure NGINX and Railo. 

    The inclusion or exclusion of any part, depends on the parameters sent to the script.

    OPTIONS
        -v Version
            Prints the "ffss" current version.

        -h Help
            Prints the synopsis and a list of the most commonly used commands.
               
        -t Temporary path
            Specify the temporary path for downloads. This directory will be removed after the script is complete
            Default: /data/tmp

        -w Webroot path
            Specify the webroot path
            Default: /webroot

        -m Install with MySQL
            Install MySQL as your database engine.

        -o Install with MongoDB
            Install MongoDB as your database engine.

        -p Default password
            Set default password for Railo Admin, MySQL root user, Mongo root user
            Default: F@ct0ry123

        -d Domain or host
            Domain or hostname for the default install
            Default: flukyfactory.com

    AUTHORS
    This script was done for Fluky Factory by Luis Matute and Douglas Barahona

EOF
    exit 1
}

##Display command usage function##
exit_usage () {
    cat <<EOF
    COMMAND USAGE
    ffss.sh [-v] [-h] [-t [=<path>]] [-w [=<path>]] [-m] [-o] [-p [=<value>]] -d [=<value>]

    For more help run: ffss.sh -h
EOF
    exit 1
}

##Display command version function##
exit_version() {
    cat <<EOF
    $SCRIPT_NAME Version $VERSION
    `uname -mrs`

    Copyright (C) 2013 Fluky Factory S de R. L. de C. V.
    Unauthorized use or distribution of this software my be penalized by the competent authorities
EOF
    exit 1
}

##read and process arguments##
while getopts ":t:w:p:d: movh" opt
do
    case $opt in
        v ) exit_version ;;
        h ) exit_help ;;
        t ) TEMP_DIR=$OPTARG ;;
        w ) WEBROOT_DIR=$OPTARG ;;
        m ) INSTALL_MYSQL=1 ;;
        o ) INSTALL_MONGODB=1 ;;
        p ) DEFAULT_PASSWORD=$OPTARG ;;
        d ) DOMAIN=$OPTARG ;;
        \? ) echo "Invalid option -$OPTARG"
            exit_usage ;;
        : ) echo "Option -$OPTARG requires an argument"
            exit_usage ;;
    esac
done
shift $((OPTIND-1))

##Display usage if unaccepted characters are type in##
if [ -n "$1" ]; then
    exit_usage
fi

##Update OS##
echo "Updating OS to the latest version"
yum update -y

##Create Temp Dir##
echo "Creating temporary directory" $TEMP_DIR
mkdir -p $TEMP_DIR

##Create webroot dir##
echo "Creating webroot directory" $WEBROOT_DIR"/default"
mkdir -p $WEBROOT_DIR/default

##Install Git## 
echo "Installing Git and wget"
yum -y install git wget

##Install NginX and Start the service##
echo "Downloading and installing NGINX repository"
wget -P $TEMP_DIR http://nginx.org/packages/centos/6/noarch/RPMS/nginx-release-centos-6-0.el6.ngx.noarch.rpm
rpm -ivh $TEMP_DIR/nginx-release-centos-6-0.el6.ngx.noarch.rpm
yum -y install nginx
echo "Starting NGINX service"
service nginx start
chkconfig nginx on

##Install Railo##
echo "Downloading and installing Railo"
wget http://www.getrailo.org/down.cfm?item=/railo/remote/download/4.1.1.009/tomcat/linux/railo-4.1.1.009-pl0-linux-x64-installer.run -O $TEMP_DIR/railo.run
chmod 775 $TEMP_DIR/railo.run
$TEMP_DIR/railo.run --mode unattended --railopass $DEFAULT_PASSWORD --installconn false

##Install and setup MySQL if selected##
if [[ INSTALL_MYSQL -eq 1 ]]; then
    echo "Installing MySQL"
    yum -y install mysql mysql-server
    ##Start and setup MySQL##
    echo "Starting and setting up MySQL"
    service mysqld start
    chkconfig mysqld on
    mysqladmin -u root password $DEFAULT_PASSWORD
    mysql -u root -p"$DEFAULT_PASSWORD" -e "GRANT ALL PRIVILEGES ON *.* TO root@'%' IDENTIFIED BY '$DEFAULT_PASSWORD'"
fi

##Install and setup MongoDB if selected##
if [[ INSTALL_MONGODB -eq 1 ]]; then
    echo "Installing MongoDB"
    cat <<EOF > /etc/yum.repos.d/mongodb.repo
[mongodb]
name=MongoDB Repository
baseurl=http://downloads-distro.mongodb.org/repo/redhat/os/x86_64/
gpgcheck=0
enabled=1
EOF
    yum -y install mongo-10gen mongo-10gen-server
    ##Installing Mongo drivers for Railo
    mkdir -p $WEBROOT_DIR/default/WEB-INF/railo/lib
    echo "Downloading MongoDB drivers for Railo"
    wget https://github.com/marcesher/cfmongodb/blob/develop/lib/cfmongodb.jar -O $WEBROOT_DIR/default/WEB-INF/railo/lib/cfmongodb.jar
    chmod 664 $WEBROOT_DIR/default/WEB-INF/railo/lib/cfmongodb.jar
    wget https://github.com/marcesher/cfmongodb/blob/develop/lib/mongo-2.10.1.jar -O $WEBROOT_DIR/default/WEB-INF/railo/lib/mongo-2.10.1.jar
    chmod 664 $WEBROOT_DIR/default/WEB-INF/railo/lib/mongo-2.10.1.jar
    wget https://github.com/marcesher/cfmongodb/blob/develop/lib/mongo-java-driver-2.11.1.jar -O $WEBROOT_DIR/default/WEB-INF/railo/lib/mongo-java-driver-2.11.1.jar
    chmod 664 $WEBROOT_DIR/default/WEB-INF/railo/lib/mongo-java-driver-2.11.1.jar
    wget https://github.com/marcesher/cfmongodb/blob/develop/lib/mxunit-ant.jar -O $WEBROOT_DIR/default/WEB-INF/railo/lib/mxunit-ant.jar
    chmod 664 $WEBROOT_DIR/default/WEB-INF/railo/lib/mxunit-ant.jar
    echo "Creating DB directory and starting service"
    mkdir -p /data/db/
    service mongod start
    chkconfig mongod on
fi

##Removing temp dir##
echo "Removing temporary directory"
rm -rf $TEMP_DIR

##Backup and copy new /etc/nginx/conf.d/default.conf##
echo "Backing up and coping new /etc/nginx/conf.d/default.conf"
cp -p /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/defaultconf.bak
cat <<EOF > /etc/nginx/conf.d/default.conf
#################################################
## default.conf Settings Modified by ffss.sh
## $TODAY
#################################################
server {
  listen 80;
	server_name $DOMAIN;
	root $WEBROOT_DIR/default;
	index index.cfm;
	access_log /var/log/default_access.log;
	error_log /var/log/default_error.log;

  #if ($http_host != "localhost") {
    #rewrite ^ http://localhost$request_uri permanent;
  #}

	include railo.conf;
}
EOF

##Creating Railo Conf /etc/nginx/railo.conf##
echo "Creating railo.conf /etc/nginx/railo.conf"
cat <<EOF > /etc/nginx/railo.conf
#################################################
## railo.conf Added by ffss.sh
## $TODAY
#################################################
location / {
		  # Rewrite rules and other criterias can go here
		  # Remember to avoid using if() where possible (http://wiki.nginx.org/IfIsEvil)
		  #try_files \$uri \$uri/ @rewrites;
}

location @rewrites {
        # Can put some of your own rewrite rules in here
        # for example rewrite ^/~(.*)/(.*)/? /users/\$1/\$2 last;
        rewrite ^/(.*)? /index.cfm/\$1 last;
        rewrite ^ /index.cfm last;
}

location ~* /railo-context/admin/ {
        internal;
        proxy_pass      http://127.0.0.1:8888;
        proxy_redirect off;
        proxy_set_header Host \$host;
        proxy_set_header X-Forwarded-Host \$host;
        proxy_set_header X-Forwarded-Server \$host;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Real-IP \$remote_addr;
}

#location ~*  \.(jpg|jpeg|png|gif|ico|css|js|woff)$ {
		  #expires 10d;
#}

location ~* /secret/location/ {
        rewrite ^/secret/location/(.*)? /railo-context/admin/\$1;
}

# Main Railo proxy handler
location ~ \.(cfm|cfml|cfc|jsp|cfr)(.*)$ {
		proxy_pass http://127.0.0.1:8888;
  	  	proxy_redirect off;
	  	proxy_set_header Host \$host;
	  	proxy_set_header X-Forwarded-Host \$host;
		proxy_set_header X-Forwarded-Server \$host;
		proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
	  	proxy_set_header X-Real-IP \$remote_addr;
}
EOF

##Backup and copy new /etc/nginx/nginx.conf##
echo "Backing up and coping new /etc/nginx/nginx.conf"
cp -p /etc/nginx/nginx.conf /etc/nginx/nginxconf.bak
cat <<EOF > /etc/nginx/nginx.conf
#################################################
## nginx.conf Settings Modified by ffss.sh
## $TODAY
#################################################
user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;


events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '\$remote_addr - \$remote_user [\$time_local] "\$request" '
                      '\$status \$body_bytes_sent "$http_referer" '
                      '"\$http_user_agent" "\$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        	on;
    tcp_nopush     	on;

    keepalive_timeout  30;

    ##################
    #gzip compression#
    ##################
    gzip on;
    gzip_disable "msie6";
    # gzip_static on;
    gzip_min_length 1400;
    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 9;
    gzip_buffers 16 8k;
    gzip_http_version 1.1;
    gzip_types text/plain text/css image/png image/gif image/jpg application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript applicacion/x-font-ttf font/opentype application/x;

    server_names_hash_bucket_size 64;
    include /etc/nginx/conf.d/*.conf;
}
EOF

##Backup and copy new /opt/railo/tomcat/conf/server.xml##
echo "Backing up and coping new /opt/railo/tomcat/conf/server.xml"
cp -p /opt/railo/tomcat/conf/server.xml /opt/railo/tomcat/conf/serverxml.bak
cat <<EOF > /opt/railo/tomcat/conf/server.xml
<?xml version='1.0' encoding='utf-8'?>
<!--
  Licensed to the Apache Software Foundation (ASF) under one or more
  contributor license agreements.  See the NOTICE file distributed with
  this work for additional information regarding copyright ownership.
  The ASF licenses this file to You under the Apache License, Version 2.0
  (the "License"); you may not use this file except in compliance with
  the License.  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->
<!-- Note:  A "Server" is not itself a "Container", so you may not
     define subcomponents such as "Valves" at this level.
     Documentation at /docs/config/server.html
 -->
<Server port="8005" shutdown="SHUTDOWN">
  <!-- Security listener. Documentation at /docs/config/listeners.html
  <Listener className="org.apache.catalina.security.SecurityListener" />
  -->
  <!--APR library loader. Documentation at /docs/apr.html -->
  <Listener className="org.apache.catalina.core.AprLifecycleListener" SSLEngine="on" />
  <!--Initialize Jasper prior to webapps are loaded. Documentation at /docs/jasper-howto.html -->
  <Listener className="org.apache.catalina.core.JasperListener" />
  <!-- Prevent memory leaks due to use of particular java/javax APIs-->
  <Listener className="org.apache.catalina.core.JreMemoryLeakPreventionListener" />
  <Listener className="org.apache.catalina.mbeans.GlobalResourcesLifecycleListener" />
  <Listener className="org.apache.catalina.core.ThreadLocalLeakPreventionListener" />

  <!-- Global JNDI resources
       Documentation at /docs/jndi-resources-howto.html
  -->
  <GlobalNamingResources>
    <!-- Editable user database that can also be used by
         UserDatabaseRealm to authenticate users
    -->
    <Resource name="UserDatabase" auth="Container"
              type="org.apache.catalina.UserDatabase"
              description="User database that can be updated and saved"
              factory="org.apache.catalina.users.MemoryUserDatabaseFactory"
              pathname="conf/tomcat-users.xml" />
  </GlobalNamingResources>

  <!-- A "Service" is a collection of one or more "Connectors" that share
       a single "Container" Note:  A "Service" is not itself a "Container", 
       so you may not define subcomponents such as "Valves" at this level.
       Documentation at /docs/config/service.html
   -->
  <Service name="Catalina">
  
    <!--The connectors can use a shared executor, you can define one or more named thread pools-->
    <!--
    <Executor name="tomcatThreadPool" namePrefix="catalina-exec-" 
        maxThreads="150" minSpareThreads="4"/>
    -->
    
    
    <!-- A "Connector" represents an endpoint by which requests are received
         and responses are returned. Documentation at :
         Java HTTP Connector: /docs/config/http.html (blocking & non-blocking)
         Java AJP  Connector: /docs/config/ajp.html
         APR (HTTP/AJP) Connector: /docs/apr.html
         Define a non-SSL HTTP/1.1 Connector on port 8080
    -->
    <Connector port="8888" protocol="HTTP/1.1" 
               connectionTimeout="20000" 
               redirectPort="8443" />
    <!-- A "Connector" using the shared thread pool-->
    <!--
    <Connector executor="tomcatThreadPool"
               port="8080" protocol="HTTP/1.1" 
               connectionTimeout="20000" 
               redirectPort="8443" />
    -->           
    <!-- Define a SSL HTTP/1.1 Connector on port 8443
         This connector uses the JSSE configuration, when using APR, the 
         connector should be using the OpenSSL style configuration
         described in the APR documentation -->
    <!--
    <Connector port="8443" protocol="HTTP/1.1" SSLEnabled="true"
               maxThreads="150" scheme="https" secure="true"
               clientAuth="false" sslProtocol="TLS" />
    -->

    <!-- Define an AJP 1.3 Connector on port 8009 -->
    <Connector port="8009" protocol="AJP/1.3" redirectPort="8443" />


    <!-- An Engine represents the entry point (within Catalina) that processes
         every request.  The Engine implementation for Tomcat stand alone
         analyzes the HTTP headers included with the request, and passes them
         on to the appropriate Host (virtual host).
         Documentation at /docs/config/engine.html -->

    <!-- You should set jvmRoute to support load-balancing via AJP ie :
    <Engine name="Catalina" defaultHost="localhost" jvmRoute="jvm1">         
    --> 
    <Engine name="Catalina" defaultHost="127.0.0.1">
    <Valve className="org.apache.catalina.valves.RemoteIpValve"  />

      <!--For clustering, please take a look at documentation at:
          /docs/cluster-howto.html  (simple how to)
          /docs/config/cluster.html (reference documentation) -->
      <!--
      <Cluster className="org.apache.catalina.ha.tcp.SimpleTcpCluster"/>
      -->        

      <!-- Use the LockOutRealm to prevent attempts to guess user passwords
           via a brute-force attack -->
      <Realm className="org.apache.catalina.realm.LockOutRealm">
        <!-- This Realm uses the UserDatabase configured in the global JNDI
             resources under the key "UserDatabase".  Any edits
             that are performed against this UserDatabase are immediately
             available for use by the Realm.  -->
        <Realm className="org.apache.catalina.realm.UserDatabaseRealm"
               resourceName="UserDatabase"/>
      </Realm>

      <Host name="127.0.0.1"  appBase="webapps"
            unpackWARs="true" autoDeploy="true">

        <!-- SingleSignOn valve, share authentication between web applications
             Documentation at: /docs/config/valve.html -->
        <!--
        <Valve className="org.apache.catalina.authenticator.SingleSignOn" />
        -->

        <!-- Access log processes all example.
             Documentation at: /docs/config/valve.html
             Note: The pattern used is equivalent to using pattern="common" -->
        <Valve className="org.apache.catalina.valves.AccessLogValve" directory="logs"  
               prefix="localhost_access_log." suffix=".txt"
               pattern="%h %l %u %t &quot;%r&quot; %s %b" resolveHosts="false"/>
    
    <!-- visit modcfml.org for details on mod_cfml configuration options -->
    <Valve className="mod_cfml.core"
        loggingEnabled="false"
        waitForContext="3"
        maxContexts="200"
        timeBetweenContexts="30000"
        />  
      </Host>

      <Host name="localhost" appBase="webapps">
        <Context path="" docBase="$WEBROOT_DIR/default" />
      </Host>

      <Host name="$DOMAIN" appBase="webapps">
        <Context path="" docBase="$WEBROOT_DIR/default" />
      </Host>

      <!--
        Add additional VIRTUALHOSTS by copying the following example config:
        REPLACE:
        [ENTER DOMAIN NAME] with a domain, IE: "mysite.com"
        [ENTER SYSTEM PATH] with your web site's base directory. IE: /home/user/public_html/ or C:\websites\www.mysite.com\ etc...
    [ENTER DOMAIN ALIAS] with any domain that also points to this same site/directory. You can repeat this tag as often as needed. 
        Don't forget to remove comments!
      -->
      <!--
    EXAMPLE HOST ENTRY:
        <Host name="getrailo.org" appBase="webapps">
             <Context path="" docBase="/var/sites/getrailo.org" />
         <Alias>www.getrailo.org</Alias>
         <Alias>my.getrailo.org</Alias>
        </Host>
    
    HOST ENTRY TEMPLATE:
        <Host name="[ENTER DOMAIN NAME]" appBase="webapps">
             <Context path="" docBase="[ENTER SYSTEM PATH]" />
         <Alias>[ENTER DOMAIN ALIAS]</Alias>
        </Host>
      -->


    </Engine>
  </Service>
</Server>
EOF

##Restat Services
echo "Restarting Services"
/opt/railo/railo_ctl restart
service nginx restart

echo "*********************"
echo "*  Finished Script  *"
echo "*********************"