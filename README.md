# FW/1 Skeleton
--------------

This is a boilerplate or starting point for any project. It contains the following:

* Public system which is the actual website boilerplate
* Admin system with basic user management
* Production Server Initialization Script which installs NGINX, Railo, MongoDB, MySQL, Git. (init.sh)
* HTML5 boilerplate
* FW/1 boilerplate with Subsystems
* Basic Layout setup with LESS
* Basic JS setup with jQuery
* Git Auto/Manual Deployments
* Error logging to SplunkStorm
* Transactional Emails with MandrillApp


## Quick Start
--------------
For a quick start, just modify configuration files in `/core/config/` with the custom data for the project (eg. DSN, password, git parameter, keys, etc.), and you are set to start

## Environment Names
--------------
If this is changed, make sure to update the way `/views/git/deploy.cfm` calls the Email List

* Development: "dev"
* Staging: "dev"
* Production: "prod"

## Notes

Since we are using cfmongo, we need to add the 4 .jar files in the following link, to the lib/ directory in the instance or the server directory
https://github.com/marcesher/cfmongodb/tree/develop/lib