<cfcomponent extends="core.fw1.framework">

	<!--- Application Settings --->
		<cfinclude template="/core/config/fw1.ini.cfm" /> 		<!--- FW1 Settings --->
		<cfinclude template="/core/config/environment.cfm" /> 	<!--- FW1 Environment Settings --->
		<cfinclude template="/core/config/subsystem.cfm" /> 	<!--- FW1 Subsystem Settings --->
		<cfinclude template="/core/config/routes.cfm" /> 		<!--- FW1 Routes --->
		<cfset this.name 				= "Skeleton" />
		<cfset this.applicationTimeout 	= createTimespan(7,0,0,0) />
		<cfset this.sessionManagement 	= true />
		<cfset this.sessionTimeout 		= createTimespan(0,0,30,0) />
		<cfset this.bf 					= createObject("component", "core.fw1.ioc").init('/common/services, /admin/services') /><!---DI/1--->

	<!--- Application Start --->
		<cffunction name="setupApplication"
		hint="Application Settings according to each subsystem. We call 2 includes, one for the
		common settings, and the second, for each subsystem app settings">
			<cfinclude template="/common/appcfc/setupApplication.cfm" />
			<cfinclude template="/#getSubsystem()#/appcfc/setupApplication.cfm" />
		</cffunction>

	<!--- Setup for each Request --->
		<cffunction name="setupRequest"
		hint="This method will be called on every request. We are going to include any subsystem
		specific logic here">
			<cfinclude template="/common/appcfc/setupRequest.cfm" />
			<cfinclude template="/#getSubsystem()#/appcfc/setupRequest.cfm" />
		</cffunction>

	<!--- Setup for each Session --->
		<cffunction name="setupSession"
		hint="This method will setup the session. We are going to include any subsystem
		specific logic here">
			<cfinclude template="/#getSubsystem()#/appcfc/setupSession.cfm" />
		</cffunction>

	<!--- Subsystems Initializer --->
		<cffunction name="setupSubsystem"
		hint="This method makes the setup for each subsystem.">
			<cfargument name="subsystem" type="string" />
			<cfset setSubsystemBeanFactory(arguments.subsystem, this.bf) />
		</cffunction>

	<!--- On Application End --->
		<cffunction name="onApplicationEnd" hint="Close Mongo connection">
    		<cfargument name="ApplicationScope" required=true/>
    		<cfset ApplicationScope.mongo.close() />
    		<cflog file="#ApplicationScope.applicationname#" type="Information" text="Application #ApplicationScope.name# Ended">
		</cffunction>

	<!--- Get Environment of Application (Local, Testing, Production) --->
		<cffunction name="getEnvironment">
			<cfif findNocase("local",CGI.SERVER_NAME) OR findNocase("dev",CGI.SERVER_NAME) >
				<cfreturn "dev" />
			<cfelse>
				<cfreturn "prod" />
			</cfif>
		</cffunction>

	<!--- Making Utilities Functions Globally Available --->
		<cfset structAppend(
			url,
			createObject( "component", "common.services.utilities" )
			) />

</cfcomponent>