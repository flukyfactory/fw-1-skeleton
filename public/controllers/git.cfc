<cfcomponent displayname="main" hint="I Handle the data for the Deployments from git">

	<cffunction name="init" access="public" output="false" hint="Constructor">
		<cfargument name="fw" />
		<cfset variables.fw = fw />
		<cfreturn this />
	</cffunction>


	<cffunction name="deploy" access="public" returntype="void">
		<cfset rc.errors = 0 />
		<cfset rc.errorMessage = "true" />
		<!--- String of Arguments passed to the shell script (Porject Name, Environment, Git Branch)--->
		<cfset rc.arguments = "#application.env.settings.git.project_name# #application.env.name# #application.env.settings.git.branch#" />
	</cffunction>

</cfcomponent>