<cfcomponent displayname="main">

	<cffunction name="init" access="public" output="false" hint="Constructor">
		<cfargument name="fw" />
		<cfset variables.fw = fw />
		<cfreturn this />
	</cffunction>


	<cffunction name="default" access="public" returntype="void"></cffunction>

</cfcomponent>