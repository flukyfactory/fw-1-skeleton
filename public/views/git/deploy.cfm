<cfparam name="URL.password" default="" />
<cfset git_response = "Nothing To Report" />
<cfoutput>
	<link rel="stylesheet" href="/#getSubsystem()#/assets/css/error-page.css">
	<div class="container git">
		<cfif URL.password IS application.env.settings.password>

			<h2>Updating server from BitBucket</h2>
			<cftry>
				<!--- Execute shell script that does the deployment magic --->
				<cfexecute 	name="#ExpandPath("./")##getSubsystem()#/views/git/git-updateWeb.sh"
			     			variable="git_response"
			     			arguments="#rc.arguments#"
			     			timeout=100></cfexecute>

				<cfcatch type="any">
					<cfset RC.errors = 1 />
					<cfset RC.errorMessage = '#cfcatch.message#, #cfcatch.detail#' />
				</cfcatch>
			</cftry>

			<!--- Send Email Notification (Success or Error) --->
			<cfset copy = "" />
			<cfif RC.errors NEQ 1>
				<cfsavecontent
				    variable = "copy">
				    <!--- Success --->
					<h3>Deployment was <strong>Successfull</strong></h3>

					<p>The deployment was made at <strong>#DateFormat(now(), 'dd/mmm/yyyy')# - #TimeFormat(now(),'hh:mm tt')#</strong></p>
					<p>Here are the details:</p>
					<pre>#git_response#</pre>
					<p><strong>Process Complete</strong></p>
				</cfsavecontent>
			<cfelse>
				<cfsavecontent
				    variable = "copy">
				    <!--- Failed --->
					<h3>Deployment was <strong><em>Unsuccessful</em></strong></h3>

					<p>The deployment failed at <strong>#DateFormat(now(), 'dd/mmm/yyyy')# - #TimeFormat(now(),'hh:mm tt')#</strong></p>
					<p>Here are the details:</p>
					<pre>#RC.errorMessage#</pre>
				</cfsavecontent>
			</cfif>

			<cfsavecontent
			    variable = "emailHTML">
			    <cfinclude template="/emails/template/inner-email.cfm" />
			</cfsavecontent>

			<cfset emailContacts = arrayNew(1) />
			<cfset contactInfo = structNew() />
			<cfset contactInfo['name'] = 'Luis Matute' />
			<cfset contactInfo['email'] = 'lmatute@flukyfactory.com' />
			<cfset arrayAppend(emailContacts, contactInfo) />
			<cfset contactInfo = structNew() />
			<cfset contactInfo['name'] = 'Lester Barahona' />
			<cfset contactInfo['email'] = 'lbarahona@flukyfactory.com' />
			<cfset arrayAppend(emailContacts, contactInfo) />

			<!--- Send Email Thru Mandrill --->
			<cfset sendMail = getBeanFactory().getBean('mandrillAPI').send(
				<!--- Key --->
				"#application.env.settings.keyMandrillAPI#",
				<!--- HTML --->
				"#emailHTML#",
				<!--- Text --->
				"#git_response#",
				<!--- Subject --->
				"[Fluky] New Deployment at #DateFormat(now(), 'dd/mmm/yyyy')# #TimeFormat(now(),'hh:mm tt')#",
				<!--- From Email --->
				"info@flukyfactory.com",
				<!--- From Name --->
				"Fluky Factory Site",
				<!--- contacts info --->
				#emailContacts#,
				<!--- Track Opens --->
				true,
				<!--- Track Clicks --->
				true
			) />

			<p>--- Message from Git ----</p>
			<pre>#git_response#</pre>
			<p>--- End Message from Git ----</p>

			<p><strong>Process Complete</strong></p>

		<cfelse>
			<h2>Permission Denied :(</h2>
			<p>We are sorry! Password is required in order to perform this action.</p>
		</cfif>
	</div>
</cfoutput>