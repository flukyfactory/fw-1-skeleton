#!/bin/sh
#
# Description:
#	This script will roll back to last commit
#	Script will receive 3 Arguments (Project Name, Environment, Branch)
#	
# Luis Matute
#

PROJECT_NAME=$1
ENVIRNOMENT=$2
BRANCH=$3

cd /webroot/$PROJECT_NAME/$ENVIRNOMENT
git reset --hard HEAD^
git clean
echo "#------------------------------------------#"
echo "# Code has been rolled back to last commit #"
echo "#------------------------------------------#"