/*
// Common JS
// Date: December 2013
// Developers:
// 	Adolfo Gutierrez - agutierrez@flukyfactory.com
// 	Luis Matute      - lmatute@flukyfactory.com
// Description:
//	Code here is available site wide
// 	Note: jQuery is available, so we dont need to
//	declare it as a dependency
// --------------------------------------------------
*/

// This is the definition of the module
	define('common', function(){
		$(function () {
			console.log("[" + app.system + "-" + app.section + "]: Initiated");
			_.legacy();
	        _.gaSetup('UA-XXXXX-X');
	        bind_events();
		});
	});

// General event binding
	function bind_events() {}