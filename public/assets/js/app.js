/*
// Common JS
// Date: November 2013
// Developers:
// 	Adolfo Gutierrez - agutierrez@flukyfactory.com
// 	Luis Matute      - lmatute@flukyfactory.com
// Description:
// 	This is the first JS file that's loaded.
// 	Takes care of the require.config which has
// 	the js paths and at the end calls the common module
// -----------------------------------------------------
*/

// Rule of thumb:
// 	Define: If you want to declare a module other parts of your application will depend on.
// 	Require: If you just want to load and use stuff.

require.config({
	baseUrl: '/' + app.system + '/assets/js',
	// urlArgs: 'bust=2',
	paths: {
		// The Libraries we use
		jquery: [
            '//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min', // google cdn
            'libs/jquery.min' // fallback
        ],
        util: 		'libs/utilities',
        common: 	'modules/common'
	},
	shim: {
		util: ['jquery'],
		common: ['jquery', 'util']
	}
});

// Defining global module with jQuery dependency and requiring the common js
define(['common'], function() {});