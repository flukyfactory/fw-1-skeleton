#!/bin/sh
#
#	Initialization Script
#	Written by: Luis Matute & Douglas Barahona
#
#	For RHEL 6 x64
# 	Basic Service Setup for Git, MySQL, Railo and NGINX
#	Config files for Railo and NGINX (set to localhost)
#

DOWNLOAD_DIR="/data"
WEBROOT_DIR="/webroot"
TODAY=`date '+%d-%m-%Y'`
if [[ -n "$1" ]] ; then
	DEFAULT_PASSWORD=$1
else
	DEFAULT_PASSWORD="F@ct0ry123"
fi

# Update Server & installing required packages
yum update -y
yum install git mysql mysql-server -y

# Create Directories
mkdir -p $DOWNLOAD_DIR
mkdir -p $WEBROOT_DIR/default

# Install NginX and Starting it
wget -P $DOWNLOAD_DIR http://nginx.org/packages/centos/6/noarch/RPMS/nginx-release-centos-6-0.el6.ngx.noarch.rpm
rpm -ivh $DOWNLOAD_DIR/nginx-release-centos-6-0.el6.ngx.noarch.rpm
yum install nginx -y
service nginx start
chkconfig nginx on

# Install Railo
wget http://www.getrailo.org/down.cfm?item=/railo/remote/download/4.0.4.001/tomcat/linux/railo-4.0.4.001-pl2-linux-x64-installer.run -O $DOWNLOAD_DIR/railo.run
chmod 775 $DOWNLOAD_DIR/railo.run
$DOWNLOAD_DIR/railo.run --mode unattended --tomcatpass $DEFAULT_PASSWORD --installconn false

# MySQL Setup
service mysqld start
chkconfig mysqld on
mysqladmin -u root password $DEFAULT_PASSWORD
mysql -u root -p"$DEFAULT_PASSWORD" -e "GRANT ALL PRIVILEGES ON *.* TO root@'%' IDENTIFIED BY '$DEFAULT_PASSWORD'"

# Erasing $DOWNLOAD_DIR Content
echo "Removing Installation Directory"
rm -rf $DOWNLOAD_DIR

# ReWritting /etc/nginx/conf.d/default.conf
echo "ReWritting /etc/nginx/conf.d/default.conf"
cp -p /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/defaultconf.bak

cat <<EOF > /etc/nginx/conf.d/default.conf
#################################################
## default.conf Settings Modified by init.sh
## $TODAY
#################################################
server {
  listen 80;
	server_name localhost;
	root "$WEBROOT_DIR"/default;
	index index.cfm;
	access_log /var/log/default_access.log;
	error_log /var/log/default_error.log;

  #if ($http_host != "localhost") {
    #rewrite ^ http://localhost$request_uri permanent;
  #}

	include railo.conf;
}
EOF

# Creating Railo Conf /etc/nginx/railo.conf
echo "Creating railo.conf /etc/nginx/railo.conf"
cat <<EOF > /etc/nginx/railo.conf
#################################################
## railo.conf Added by init.sh
## $TODAY
#################################################
location / {
		  # Rewrite rules and other criterias can go here
		  # Remember to avoid using if() where possible (http://wiki.nginx.org/IfIsEvil)
		  #try_files \$uri \$uri/ @rewrites;
}

location @rewrites {
        # Can put some of your own rewrite rules in here
        # for example rewrite ^/~(.*)/(.*)/? /users/\$1/\$2 last;
        rewrite ^/(.*)? /index.cfm/\$1 last;
        rewrite ^ /index.cfm last;
}

location ~* /railo-context/admin/ {
        internal;
        proxy_pass      http://127.0.0.1:8888;
        proxy_redirect off;
        proxy_set_header Host \$host;
        proxy_set_header X-Forwarded-Host \$host;
        proxy_set_header X-Forwarded-Server \$host;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Real-IP \$remote_addr;
}

#location ~*  \.(jpg|jpeg|png|gif|ico|css|js|woff)$ {
		  #expires 10d;
#}

location ~* /secret/location/ {
        rewrite ^/secret/location/(.*)? /railo-context/admin/\$1;
}

# Main Railo proxy handler
location ~ \.(cfm|cfml|cfc|jsp|cfr)(.*)$ {
		proxy_pass http://127.0.0.1:8888;
  	  	proxy_redirect off;
	  	proxy_set_header Host \$host;
	  	proxy_set_header X-Forwarded-Host \$host;
		proxy_set_header X-Forwarded-Server \$host;
		proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
	  	proxy_set_header X-Real-IP \$remote_addr;
}
EOF

# ReWritting /etc/nginx/nginx.conf
echo "ReWritting /etc/nginx/nginx.conf"
cp -p /etc/nginx/nginx.conf /etc/nginx/nginxconf.bak

cat <<EOF > /etc/nginx/railo.conf
#################################################
## nginx.conf Settings Modified by init.sh
## $TODAY
#################################################
user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;


events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '\$remote_addr - \$remote_user [\$time_local] "\$request" '
                      '\$status \$body_bytes_sent "$http_referer" '
                      '"\$http_user_agent" "\$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        	on;
    tcp_nopush     	on;

    keepalive_timeout  30;

    ##################
    #gzip compression#
    ##################
    gzip on;
    gzip_disable "msie6";
    # gzip_static on;
    gzip_min_length 1400;
    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 9;
    gzip_buffers 16 8k;
    gzip_http_version 1.1;
    gzip_types text/plain text/css image/png image/gif image/jpg application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript applicacion/x-font-ttf font/opentype application/x;

    server_names_hash_bucket_size 64;
    include /etc/nginx/conf.d/*.conf;
}
EOF

# Restarting Services
echo "Restarting Services"
/opt/railo/railo_ctl restart
service nginx restart

echo "*********************"
echo "*  Finished Script  *"
echo "*********************"