<cfcomponent name="userService" accessors="true">

	<cffunction name="init">
		<cfset variables.MongoCollectionUsers 		= 'users' />
		<cfset variables.MongoUsers 				= application.mongo.getDBCollection( variables.MongoCollectionUsers ) />
	</cffunction>

	<cffunction name="validateLogin" access="public" returntype="array">
		<cfargument name="username" require="true" type="string" />
		<cfargument name="password" require="true" type="string" />

		<cfset arrayUser = ArrayNew(1) />
		<cftry>
			<cfset tempStruct = variables.MongoUsers.findOne({ "username": "#ARGUMENTS.username#" }) />
			<cfset ArrayAppend(arrayUser, tempStruct) />
			<cfcatch type="any">
				<cflog file="#application.settings.companyName#" type="error" text='date="#dateFormat(now(), 'mm-dd-yyyy')#" time="#timeFormat(now(), 'hh:mm:ss')#" message="#cfcatch.message#" detail="#cfcatch.detail#"'>
			</cfcatch>
		</cftry>

		<cfreturn arrayUser />
	</cffunction>

	<cffunction name="getUsers" access="public" returntype="array">
		<cfargument name="id" type="string" required="false" default="0" />

		<cfset arrayUsers = ArrayNew(1) />
		<cftry>
			<cfif ARGUMENTS.id NEQ "0">
				<cfset tempStruct = variables.MongoUsers.findById(ARGUMENTS.id,variables.MongoCollectionUsers) />
				<cfset ArrayAppend(arrayUsers, tempStruct) />
			<cfelse>
				<cfset arrayUsers = variables.MongoUsers.query().find().asArray() />
			</cfif>

			<cfcatch type="any">
				<cflog file="#application.settings.companyName#" type="error" text='date="#dateFormat(now(), 'mm-dd-yyyy')#" time="#timeFormat(now(), 'hh:mm:ss')#" message="#cfcatch.message#" detail="#cfcatch.detail#"'>
			</cfcatch>
		</cftry>

		<cfreturn arrayUsers />
	</cffunction>

	<cffunction name="save" access="public" returntype="struct" hint="Saves new user to DB">

		<cfargument name="user_name" type="string" required="true" />
		<cfargument name="user_fname" type="string" required="true" />
		<cfargument name="user_lname" type="string" required="true" />
		<cfargument name="user_email" type="string" required="true" />
		<cfargument name="user_dob" type="date" required="true" />
		<cfargument name="user_seniority" type="date" required="true" />
		<cfargument name="user_salary" type="string" required="false" default="0.00" />
		<cfargument name="user_phone" type="string" required="false" default="" />
		<cfargument name="user_mobile" type="string" required="false" default="" />
		<cfargument name="user_password" type="string" required="true" />

		<cfset structResult = structNew() />
		<cfset structResult.code = 1 />
		<cfset structResult.message = "" />

		<cftry>
			<cfset findUserCount = variables.MongoUsers.query().$eq("username", "#ARGUMENTS.user_name#").count() />
			<cfif findUserCount EQ 0 >
				<cfset newUser = {
				    "fname" 	: "#ARGUMENTS.user_fname#",
				    "lname" 	: "#ARGUMENTS.user_lname#",
				    "email" 	: "#ARGUMENTS.user_email#",
				    "dob" 		: "#ARGUMENTS.user_dob#",
				    "seniority" : "#ARGUMENTS.user_seniority#",
				    "salary" 	: "#ARGUMENTS.user_salary#",
				    "username" 	: "#ARGUMENTS.user_name#",
				    "phone" 	: "#ARGUMENTS.user_phone#",
				    "mobile" 	: "#ARGUMENTS.user_mobile#",
				    "password" 	: "#Hash(ARGUMENTS.user_password, 'SHA-256')#"
				} />
				<cfset variables.MongoUsers.save(newUser) />
			<cfelse>
				<cfset structResult.code = 1 />
				<cfset structResult.message = "Username Already in DB" />
			</cfif>

			<cfcatch type="any">
				<cfset structResult.code = 0 />
				<cfset structResult.message = cfcatch.message />
				<cflog file="#application.settings.companyName#" type="error" text='date="#dateFormat(now(), 'mm-dd-yyyy')#" time="#timeFormat(now(), 'hh:mm:ss')#" message="#cfcatch.message#" detail="#cfcatch.detail#"'>
			</cfcatch>
		</cftry>

		<cfreturn structResult />
	</cffunction>

	<cffunction name="update" access="public" returntype="struct">
		<cfargument name="id" 				type="string" 	required="true" />
		<cfargument name="user_name" 		type="string" 	required="true" />
		<cfargument name="user_fname" 		type="string" 	required="true" />
		<cfargument name="user_lname" 		type="string" 	required="true" />
		<cfargument name="user_email" 		type="string" 	required="true" />
		<cfargument name="user_dob" 		type="date" 	required="true" />
		<cfargument name="user_seniority" 	type="date" 	required="true" />
		<cfargument name="user_salary" 		type="string" 	required="false" default="0.00" />
		<cfargument name="user_phone" 		type="string" 	required="false" default="" />
		<cfargument name="user_mobile" 		type="string" 	required="false" default="" />
		<cfargument name="user_password" 	type="string" 	required="true" />

		<cfset structResult = structNew() />
		<cfset structResult.code = 1 />
		<cfset structResult.message = "" />

		<cftry>
			<cfset userToUpdate = variables.MongoUsers.findById(ARGUMENTS.id, variables.MongoCollectionUsers) />
			<cfset userToUpdate.fname 		= "#ARGUMENTS.user_fname#" />
			<cfset userToUpdate.lname 		= "#ARGUMENTS.user_lname#" />
			<cfset userToUpdate.email 		= "#ARGUMENTS.user_email#" />
			<cfset userToUpdate.dob 		= "#ARGUMENTS.user_dob#" />
			<cfset userToUpdate.seniority 	= "#ARGUMENTS.user_seniority#" />
			<cfset userToUpdate.salary 		= "#ARGUMENTS.user_salary#" />
			<cfset userToUpdate.username 	= "#ARGUMENTS.user_name#" />
			<cfset userToUpdate.phone 		= "#ARGUMENTS.user_phone#" />
			<cfset userToUpdate.mobile 		= "#ARGUMENTS.user_mobile#" />
			<cfset userToUpdate.password 	= "#Hash(ARGUMENTS.user_password, 'SHA-256')#" />
			<cfset variables.MongoUsers.save(userToUpdate) />

			<cfcatch type="any">
				<cfset structResult.code = 0 />
				<cfset structResult.message = cfcatch.message />
				<cflog file="#application.settings.companyName#" type="error" text='date="#dateFormat(now(), 'mm-dd-yyyy')#" time="#timeFormat(now(), 'hh:mm:ss')#" message="#cfcatch.message#" detail="#cfcatch.detail#"'>
			</cfcatch>
		</cftry>

		<cfreturn structResult />
	</cffunction>

	<cffunction name="delete" access="public" returntype="struct">
		<cfargument name="id" type="string" required="true" />

		<cfset structResult = structNew() />
		<cfset structResult.code = 1 />
		<cfset structResult.message = "" />

		<cftry>
			<cfset user = variables.MongoUsers.findById(ARGUMENTS.id,variables.MongoCollectionUsers) />
			<cfset variables.MongoUsers.remove(user) />

		<cfcatch type="any">
			<cfset structResult.code = 0 />
			<cfset structResult.message = cfcatch.message />
			<cflog file="#application.settings.companyName#" type="error" text='date="#dateFormat(now(), 'mm-dd-yyyy')#" time="#timeFormat(now(), 'hh:mm:ss')#" message="#cfcatch.message#" detail="#cfcatch.detail#"'>
		</cfcatch>
		</cftry>

		<cfreturn structResult />
	</cffunction>

</cfcomponent>