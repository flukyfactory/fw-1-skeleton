<cfoutput>
	<!DOCTYPE html>
	<html>
		<!--- HTML Head --->
		#view('includes/html_head')#

		<body>
			<!--- Site's Header --->
			#view('includes/header')#

			<div class="container">

				<div class="row-fluid">

					<div class="span12">
						<!--- Back-end Messages --->
						#view('includes/message')#

						<!--- View code goes here --->
						#body#
					</div>

				</div>

				<!--- Site's Footer --->
				#view('includes/footer')#

			</div> <!--- /.container --->

			<!--- Le JS --->
	  		#view('includes/js')#

		</body>
	</html>
</cfoutput>