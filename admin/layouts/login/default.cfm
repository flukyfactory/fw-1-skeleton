<cfoutput>
    <!DOCTYPE html>
    <html>
        <!--- HTML head --->
            #view("includes/html_head")#

        <body class="section-#getSection()#">
            <!--- View code goes here --->
                #body#
                <cfset request.layout = false>
            <!--- Scripts --->
                #view('includes/js')#
        </body>
    </html>
</cfoutput>