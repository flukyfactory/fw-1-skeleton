<cfoutput>
    <link rel="stylesheet" href="/common/assets/css/error-page.css">
    <div class="container">
        <h1>ERROR! <span>:(</span></h1>
        <p>Sorry, but an error occurred!</p>
        <p>Here is a detailed explanation:</p>

        <cfif structKeyExists( request, 'failedAction' )>
            <b>Action:</b> #request.failedAction#<br/>
        <cfelse>
            <b>Action:</b> unknown<br/>
        </cfif>
        <b>Error:</b> #request.exception.cause.message#<br/>
        <b>Type:</b> #request.exception.cause.type#<br/>
        <b>Details:</b> #request.exception.cause.detail#<br/>
    </div>
    <cfif application.env.name EQ "dev">
        <br>
        <cfdump var="#request.exception#" />
    </cfif>
</cfoutput>