﻿<cfoutput>
    <div class="container">
        <form id="form-signin" class="col-lg-6 col-lg-offset-3 panel panel-default" method="post" action="#buildURL('login.check')#">
            <h2 class="">Please sign in</h2>
            <!--- Including message that will show if there is an exception --->
            #view("includes/message")#
            <div class="form-group">
                <input type="text" class="form-control input-lg" placeholder="Username" name="username">
            </div>
            <div class="form-group">
                <input type="password" class="form-control input-lg" placeholder="Password" name="password">
            </div>
            <div class="form-group">
                <button class="btn btn-primary btn-lg btn-block" type="submit" name="submit">Login</button>
            </div>
        </form>
    </div><!--- /.container --->
</cfoutput>