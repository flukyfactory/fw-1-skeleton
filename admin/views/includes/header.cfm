﻿<cfoutput>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#buildURL('main')#">#getSubsystemConfig(getSubSystem()).name#</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li <cfif findNocase('main', rc.action)>class="active"</cfif> ><a href="#buildURL('main.default')#">Home</a></li>
                    <li <cfif findNocase('contacts', rc.action)>class="active"</cfif>><a href="#buildURL('contacts.default')#">Contacts</a></li>
                    <li <cfif findNocase('users', rc.action)>class="active"</cfif>><a href="#buildURL('users.default')#">Users</a></li>
                </ul>
                <p class="navbar-text navbar-right">
                    Logged as <a href="##" class="navbar-link">#rc.auth.username#</a> | <a href="#buildURL('login.logout')#" class="navbar-link">Logout</a>
                </p>
            </div><!--/.nav-collapse -->
        </div><!--- /.container --->
    </nav><!--- /.navbar --->
</cfoutput>