﻿<cfsilent>
	<cfset system 	= getSubsystem() />
	<cfset section 	= getSection() />
	<cfset item 	= getItem() />
    <cfset js_file 	= "/#system#/assets/js/views/" & section & "/" & item />
    <cfset js_path 	= fileExists(expandPath(js_file & ".js")) ? js_file : "/#system#/assets/js/app" />
</cfsilent>

<cfoutput>
	<!---
		Coldfusion vars to JS vars
		We are putting every js var inside the window.app
		This way we are not polluting the Global namespace
	--->
	<script>
		window.app = {
			app_name: 	"#application.applicationname#",
			system: 	"#system#",
			section: 	"#section#",
			item: 		"#item#"
		};
	</script>
	<script data-main="#js_path#" src="/common/assets/js/libs/r.js"></script>
</cfoutput>