﻿<cfoutput>
    <head>
    	<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>#rc.title#</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="FW/1 and Twitter Bootstrap 3">
        <meta name="author" content="Fluky Factory">
        <link type="text/plain" rel="author" href="/humans.txt" /><!--- Because we are humans, let's leave a mark! --->

        <link rel="stylesheet" href="/#getSubsystem()#/assets/css/master.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

    	<!-- Fav icon -->
    	<link rel="shortcut icon" href="/#getSubsystem()#/assets/img/global/favicon.png">
    </head>
</cfoutput>