<cfoutput>
    <div class="hero">
        <h2>Contacts Module</h2>
        <p>&nbsp;</p>
    </div>
    <div class="row-fluid">
        <table class="table table-striped table-condensed">
            <thead>
                <tr>
                    <th>Name</th>
                    <th class="visible-lg">Email</th>
                    <th>Message</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <cfloop array="#rc.arrayContacts#" index="contact">
                  <tr>
                    <td>#contact.name#</td>
                    <td class="visible-lg">#contact.email#</td>
                    <td class="comment">#contact.comment#</td>
                        <td class="actions" align="center">
                            <a href="##myModal-#contact._id#" data-toggle="modal" class="btn btn-primary"><i class="glyphicon-trash glyphicon"></i></a>

                            <!-- Modal -->
                            <div id="myModal-#contact._id#" class="modal fade">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel#contact._id#">Confirm Action</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Are you sure you want to delete this contact?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                            <a href="#buildURL('contacts.delete&id=#contact._id#')#" class="btn btn-primary">Yes</a>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                        </td>
                  </tr>
                </cfloop>
            </tbody>
        </table>
    </div>
</cfoutput>
