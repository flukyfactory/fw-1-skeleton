<cfoutput>
    <div class="hero">
        <h2>#rc.title#</h2>
    </div>

    <div class="row-fluid">
        <h4>Found (#ArrayLen(rc.arrayUsers)#) Records.</h4>
        <div class="btn-group">
            <a id="btn-add-user" class="btn btn-primary btn-lg" href="#buildURL('users.add')#"><i class="icon-user icon-white"></i> Add User</a>
        </div>

        <cfif ArrayLen(rc.arrayUsers) GT 0>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>User Name</th>
                        <th>Full Name</th>
                        <th align="center">Actions</th>
                    </tr>
                </thead>
                <cfloop array="#rc.arrayUsers#" index="user">
                    <tr>
                        <td>#user.username#</td>
                        <td>#user.fname# #user.lname#</td>
                        <td class="actions" align="center">
                            <a href="#buildURL('users.edit&id=#user._id#')#" class="btn btn-primary"> <i class="glyphicon glyphicon-pencil"></i></a> | 
                            <a href="##myModal#user._id#" class="btn btn-primary" data-toggle="modal"><i class="glyphicon glyphicon-trash"></i></a>

                            <!-- Modal -->
                            <div id="myModal#user._id#" class="modal fade">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel#user._id#">Confirm Action</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Are you sure you want to delete username: <strong>#user.username#</strong>?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                            <a href="#buildURL('users.delete&id=#user._id#')#" class="btn btn-primary">Yes</a>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                        </td>
                    </tr>
                </cfloop>
            </table>
        </cfif>
    </div>
</cfoutput>