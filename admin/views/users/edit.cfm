<cfoutput>
	<h1>#rc.title#</h1>

	<div class="row-fluid">
		<form id="form-add-user" class="form-horizontal form-user" action="#buildURL('users.updateUser')#" method="post">
			<input type="hidden" name="id" value="#rc.arrayUser[1]._id#">
			<!--- User Name --->
				<div class="form-group">
					<label class="control-label col-lg-2" for="input-username">User Name</label>
					<div class="col-lg-10">
				  		<input type="text" name="user_name" id="input-username" class="form-control input-lg required" value="#rc.arrayUser[1].username#">
				  	</div>
				</div>
			<!--- First Name --->
				<div class="form-group">
					<label class="control-label col-lg-2" for="input-fname">First Name</label>
					<div class="col-lg-10">
				  		<input type="text" name="user_fname" id="input-fname" class="form-control input-lg required" value="#rc.arrayUser[1].fname#">
				  	</div>
				</div>
			<!--- Last Name --->
				<div class="form-group">
					<label class="control-label col-lg-2" for="input-lname">Last Name</label>
					<div class="col-lg-10">
				  		<input type="text" name="user_lname" id="input-lname" class="form-control input-lg required" value="#rc.arrayUser[1].lname#">
				  	</div>
				</div>
			<!--- Email --->
				<div class="form-group">
					<label class="control-label col-lg-2" for="input-email">Email</label>
					<div class="col-lg-10">
				  		<input type="text" name="user_email" id="input-email" class="form-control input-lg required" value="#rc.arrayUser[1].email#">
				  	</div>
				</div>
			<!--- DOB --->
				<div class="form-group">
					<label class="control-label col-lg-2" for="input-dob">Date of Birth</label>
					<div class="col-lg-10">
				  		<input type="text" name="user_dob" id="input-dob" class="form-control input-lg required" value="#DateFormat(rc.arrayUser[1].dob, 'YYYY-MM-DD')#">
				  	</div>
				</div>
			<!--- Phone --->
				<div class="form-group">
					<label class="control-label col-lg-2" for="input-phone">Phone</label>
					<div class="col-lg-10">
				  		<input type="number" name="user_phone" id="input-phone" class="form-control input-lg" value="#rc.arrayUser[1].phone#">
				  	</div>
				</div>
			<!--- Mobile --->
				<div class="form-group">
					<label class="control-label col-lg-2" for="input-mobile">Mobile</label>
					<div class="col-lg-10">
				  		<input type="number" name="user_mobile" id="input-mobile" class="form-control input-lg" value="#rc.arrayUser[1].mobile#">
				  	</div>
				</div>
			<!--- Password --->
			  	<div class="form-group">
				    <label class="control-label col-lg-2" for="input-password1">Password</label>
				    <div class="col-lg-10">
				    	<input type="password" name="" class="form-control input-lg required" id="input-password1" value="#rc.arrayUser[1].password#">
				    </div>
			  	</div>
			<!--- Retry Password --->
			  	<div class="form-group">
				    <label class="control-label col-lg-2" for="input-password2">Re-Type Password</label>
				    <div class="col-lg-10">
				    	<input type="password" name="user_password" class="form-control input-lg required" id="input-password2" value="#rc.arrayUser[1].password#">
				    </div>
			  	</div>

			<!--- Edit User --->
			  	<div class="form-group">
				    <div class="col-lg-offset-2 col-lg-10">
				      	<button type="submit" class="btn btn-primary btn-block btn-lg submit">Edit User</button>
				    </div>
			  	</div>
		</form>
	</div>
</cfoutput>
