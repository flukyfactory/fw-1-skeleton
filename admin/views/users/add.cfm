<cfparam name="rc.suffix_id" default="" />
<cfparam name="rc.user_name" default="" />
<cfparam name="rc.user_fname" default="" />
<cfparam name="rc.user_lname" default="" />
<cfparam name="rc.user_email" default="" />
<cfparam name="rc.user_dob" default="" />a
<cfparam name="rc.user_phone" default="" />
<cfparam name="rc.user_mobile" default="" />
<cfparam name="rc.country_id" default="" />
<cfparam name="rc.user_type_id" default="" />
<cfparam name="rc.user_password" default="" />

<cfoutput>
	<h1>#rc.title#</h1>

	<div class="row-fluid">
		<form id="form-add-user" class="form-horizontal form-user" action="#buildURL('users.save')#" method="post">
			<!--- User Name --->
				<div class="form-group">
					<label class="control-label col-lg-2" for="input-username">User Name</label>
					<div class="col-lg-10">
				  		<input type="text" name="user_name" id="input-username" class="form-control input-lg required" <cfif len(rc.user_name)> value="#rc.user_name#" <cfelse> placeholder="User Name" </cfif> >
				  	</div>
				</div>
			<!--- First Name --->
				<div class="form-group">
					<label class="control-label col-lg-2" for="input-fname">First Name</label>
					<div class="col-lg-10">
				  		<input type="text" name="user_fname" id="input-fname" class="form-control input-lg required" <cfif len(rc.user_fname)> value="#rc.user_fname#" <cfelse> placeholder="First Name" </cfif> >
				  	</div>
				</div>
			<!--- Last Name --->
				<div class="form-group">
					<label class="control-label col-lg-2" for="input-lname">Last Name</label>
					<div class="col-lg-10">
				  		<input type="text" name="user_lname" id="input-lname" class="form-control input-lg required" <cfif len(rc.user_lname)> value="#rc.user_lname#" <cfelse> placeholder="Last Name" </cfif> >
				  	</div>
				</div>
			<!--- Email --->
				<div class="form-group">
					<label class="control-label col-lg-2" for="input-email">Email</label>
					<div class="col-lg-10">
				  		<input type="text" name="user_email" id="input-email" class="form-control input-lg required" <cfif len(rc.user_email)> value="#rc.user_email#" <cfelse> placeholder="Email" </cfif> >
				  	</div>
				</div>
			<!--- DOB --->
				<div class="form-group">
					<label class="control-label col-lg-2" for="input-dob">Date of Birth</label>
					<div class="col-lg-10">
				  		<input type="text" name="user_dob" id="input-dob" class="form-control input-lg datepicker required" readonly="readonly" <cfif len(rc.user_dob)> value="#rc.user_dob#" <cfelse> placeholder="DOB ( 'YYYY-MM-DD' )" </cfif> >
				  	</div>
				</div>
			<!--- Phone --->
				<div class="form-group">
					<label class="control-label col-lg-2" for="input-phone">Phone</label>
					<div class="col-lg-10">
				  		<input type="text" name="user_phone" id="input-phone" class="form-control input-lg" <cfif len(rc.user_phone)> value="#rc.user_phone#" <cfelse> placeholder="Phone" </cfif> >
				  	</div>
				</div>
			<!--- Mobile --->
				<div class="form-group">
					<label class="control-label col-lg-2" for="input-mobile">Mobile</label>
					<div class="col-lg-10">
				  		<input type="text" name="user_mobile" id="input-mobile" class="form-control input-lg" <cfif len(rc.user_mobile)> value="#rc.user_mobile#" <cfelse> placeholder="Mobile" </cfif> >
				  	</div>
				</div>
			<!--- Password --->
			  	<div class="form-group">
				    <label class="control-label col-lg-2" for="input-password1">Password</label>
				    <div class="col-lg-10">
				    	<input type="password" name="" class="form-control input-lg required" id="input-password1" <cfif len(rc.user_password)> value="#rc.user_password#" <cfelse> placeholder="Password" </cfif> >
				    </div>
			  	</div>
			<!--- Retry Password --->
			  	<div class="form-group">
				    <label class="control-label col-lg-2" for="input-password2">Re-Type Password</label>
				    <div class="col-lg-10">
				    	<input type="password" name="user_password" class="form-control input-lg required" id="input-password2" <cfif len(rc.user_password)> value="#rc.user_password#" <cfelse> placeholder="Re-Type Password" </cfif> >
				    </div>
			  	</div>

		  	<!--- Create User --->
			  	<div class="form-group">
				    <div class="col-lg-offset-2 col-lg-10">
				      	<button type="submit" class="btn btn-primary btn-block btn-lg submit">Create User</button>
				    </div>
			  	</div>
		</form>
	</div>
</cfoutput>
