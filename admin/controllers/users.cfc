<cfcomponent accessors="true">

	<!--- Dependency Injection --->
		<cfproperty name="userService" />

	<!--- Controller Init --->
		<cffunction name="init">
			<cfargument name="fw" type="any" />
			<cfset variables.fw = fw />
			<cfreturn this />
		</cffunction>

	<!--- View Methods --->
		<cffunction name="default" output="false">
			<cfset rc.title = "Users Module" />
			<!---Call Service to retieve all users--->
			<cfset arrayUsers = getUserService().getUsers(argumentCollection = rc) />
		</cffunction>

		<cffunction name="add" output="false">
			<cfset rc.title = "Add User" />
		</cffunction>

		<cffunction name="save" output="false">
			<cfparam name="rc.suffix_id" default=1 />
			<cfparam name="rc.user_name" default="" />
			<cfparam name="rc.user_fname" default="" />
			<cfparam name="rc.user_lname" default="" />
			<cfparam name="rc.user_email" default="" />
			<cfparam name="rc.user_dob" default="" />
			<cfparam name="rc.user_seniority" default="" />
			<cfparam name="rc.user_salary" default="" />
			<cfparam name="rc.user_phone" default="" />
			<cfparam name="rc.user_mobile" default="" />
			<cfparam name="rc.country_id" default=1 />
			<cfparam name="rc.user_type_id" default=1 />
			<cfparam name="rc.user_password" default="" />

			<cfset rc.msg = "" />
			<cfset rc.msgType = "danger" />

			<!---Back-end validation--->
				<cfif not len(trim(rc.user_name))>
		        	<cfset rc.msg = rc.msg&"User Name is required. <br>">
		    	</cfif>
		    	<cfif not len(trim(rc.user_fname))>
		        	<cfset rc.msg = rc.msg&"First Name is required. <br>">
		    	</cfif>
		    	<cfif not len(trim(rc.user_lname))>
		        	<cfset rc.msg = rc.msg&"Last Name is required. <br>">
		    	</cfif>
		    	<cfif not len(trim(rc.user_email))>
		        	<cfset rc.msg = rc.msg&"Email is required. <br>">
		    	</cfif>
		    	<cfif not len(trim(rc.user_dob))>
		        	<cfset rc.msg = rc.msg&"DOB is required. <br>">
		    	</cfif>
		    	<cfif not len(trim(rc.user_seniority))>
		        	<cfset rc.msg = rc.msg&"Date of Seniority is required. <br>">
		    	</cfif>

	    	<!---redirect if required fields are missing--->
	    	<cfif len(rc.msg)>
	    		<cfset _redirect("users.add","username,password,msg,msgType") />
	        <cfelse>
	        	<cfset variables.fw.service("userService.save", "result") />
	    	</cfif>

			<cfif rc.result.code GT 0>
				<cfset rc.msg = "User added successfuly" />
				<cfset rc.msgType = "success" />
				<cfset _redirect("users.default", "msg,msgType") />
			<cfelse>
				<cfset rc.msg = rc.result.message />
				<cfset _redirect("users.add", "user_type_id,country_id,user_mobile,user_phone,suffix_id,user_name,user_fname,user_lname,user_email,user_dob,user_seniority,user_salary,user_password,msg,msgType") />
			</cfif>
		</cffunction>

		<cffunction name="edit" output="false">
			<cfparam name="rc.id" default="0" />
			<cfset rc.title  = "Edit User" />
			<cfset variables.fw.service("userService.getUsers", "arrayUser") />
		</cffunction>

		<cffunction name="updateUser" output="false">
			<cfparam name="rc.user_name" default="" />
			<cfparam name="rc.user_password" default="" />

			<cfset rc.msg = "" />
			<cfset rc.msgType = "danger" />

			<!---Back-end validation--->
			<cfif not len(trim(rc.user_name))>
	        	<cfset rc.msg = rc.msg&"User Name is required. <br>">
	    	</cfif>
	    	<cfif not len(trim(rc.user_password))>
	        	<cfset rc.msg = rc.msg&"password is required. <br>">
	    	</cfif>

	    	<!---redirect if required fields are missing--->
	    	<cfif len(rc.msg)>
	    		<cfset _redirect("users.edit", "user_id,msg,msgType") />
	        <cfelse>
	        	<cfset rc.result = getUserService().update(argumentCollection = rc) />
	    	</cfif>

			<cfif rc.result.code GT 0>
				<cfset rc.msg = "User edited successfuly" />
				<cfset rc.msgType = "success" />
				<cfset _redirect("users.default", "msg,msgType") />
			<cfelse>
				<cfset rc.msg = result.message />
				<cfset rc.msgType = "danger" />
				<cfset _redirect("users.edit", "user_id,msg,msgType") />
			</cfif>
		</cffunction>

		<cffunction name="delete" output="false">
			<cfparam name="rc.id" default="" />
			<cfset rc.msg = "" />
			<cfset rc.msgType = "danger" />

			<cfif not len(trim(rc.id))>
	        	<cfset rc.msg = "No user Id was provided.">
	    	</cfif>

	    	<!---redirect if required fields are missing--->
	    	<cfif len(rc.msg)>
	    		<cfset _redirect("users.default", "msg,msgType") />
	        <cfelse>
				<cfset rc.result = getUserService().delete(argumentCollection = rc) />
			</cfif>

			<cfif rc.result.code GT 0>
				<cfset rc.msg = "User deleted successfuly." />
				<cfset rc.msgType = "success" />
				<cfset _redirect("users.default", "msg,msgType") />
			<cfelse>
				<cfset rc.message = result.message />
				<cfset rc.msgType = "danger" />
				<cfset _redirect("users.default", "msg,msgType") />
			</cfif>
		</cffunction>

</cfcomponent>