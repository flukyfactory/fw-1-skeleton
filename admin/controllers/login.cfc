﻿<cfcomponent accessors="true">

	<!--- Dependency Injection --->
		<cfproperty name="userService" />

	<!--- Controller Init --->
		<cffunction name="init" access="public" output="false" hint="Constructor">
			<cfargument name="fw" />
			<cfset variables.fw = fw />
			<cfreturn this />
		</cffunction>

	<!--- View Methods --->
		<cffunction name="login" 	access="public" returntype="void"></cffunction>

		<cffunction name="logout" 	access="public" returntype="void">
			<cfset structClear(session) />
			<cfset session.auth = structNew() />
			<cfset session.auth.isLoggedIn = false />
			<cfset session.auth.username = "Guest" />
			<cfset _redirect("login") />
		</cffunction>

		<cffunction name="check" 	access="public" returntype="void" hint="Checks if user info is correct">
			<cfset arrayUserInfo = getUserService().validateLogin(argumentCollection = rc) />

			<cfif ArrayLen(arrayUserInfo) GT 0>
				<cfif arrayUserInfo[1].password EQ Hash(rc.password, 'SHA-256')>
					<cfset session.auth = structNew() />
					<cfset session.auth.isLoggedIn = true />
					<cfset session.auth.username = arrayUserInfo[1].username />
					<cfset _redirect("main") />
				<cfelse>
					<cfset rc.msg = "Login Failed, Password is invalid" />
					<cfset rc.msgType = "danger" />
					<cfset _redirect("login","msg,msgType") />
				</cfif>
			<cfelse>
				<cfset rc.msg = "Login Failed, Username is invalid" />
				<cfset rc.msgType = "danger" />
				<cfset _redirect("login","msg,msgType") />
			</cfif>
		</cffunction>

</cfcomponent>