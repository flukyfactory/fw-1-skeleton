﻿<cfcomponent accessors="true">

    <!--- Dependency Injection --->

    <!--- Controller Init --->
        <cffunction name="init">
            <cfargument name="fw" type="any" />
            <cfset variables.fw = fw />
            <cfreturn this />
        </cffunction>

    <!--- Public Methods --->
        <cffunction name="setSession">
            <cfset session.auth = {} />
            <cfset session.auth.isLoggedIn = false />
            <cfset session.auth.username = 'Guest' />
        </cffunction>

        <cffunction name="authorize">
            <cfif !structKeyExists(session,'auth')>
                <cfset setSession() />
            </cfif>
            <cfif !session.auth.isLoggedIn &&
                  !listfindnocase( 'login', variables.fw.getSection() ) &&
                  !listfindnocase( 'main.error', variables.fw.getFullyQualifiedAction() )>
                <cfset _redirect("login") />
            </cfif>
            <cfset request.context.auth = session.auth />
        </cffunction>

</cfcomponent>