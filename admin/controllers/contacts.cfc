<cfcomponent accessors="true">

	<!--- Dependency Injection --->
		<cfproperty name="contactsService" />

	<!--- Controller Init --->
		<cffunction name="init">
			<cfargument name="fw" type="any" />
			<cfset variables.fw = fw />
			<cfreturn this />
		</cffunction>

	<!--- View Methods --->
		<cffunction name="default" output="false">
			<cfset rc.title &= "| Contacts" />
			<cfset rc.qryContacts = getContactsService().getContacts() />
		</cffunction>

		<cffunction name="delete" output="false">
			<cfparam name="rc.id" default="" />
			<cfset rc.title &= "| Delete" />
			<cfset msg = "" />
			<cfset msgType = "danger" />
			<cfif not len(trim(rc.id))>
	        	<cfset msg = "No user Id was provided.">
	    	</cfif>
	    	<!---redirect if required fields are missing--->
	    	<cfif len(msg)>
	        	<cfset _redirect("contacts.default", "msg,msgType") />
	        <cfelse>
	        	<cfset result = getContactsService().delete(argumentCollection = rc) />
			</cfif>

			<cfif result.code GT 0>
				<cfset msg = "Contact deleted successfuly." />
				<cfset msgType = "success" />
				<cfset _redirect("contacts.default", "msg,msgType") />
			<cfelse>
				<cfset message = result.message />
				<cfset msgType = "danger" />
				<cfset _redirect("contacts.default", "msg,msgType") />
			</cfif>
		</cffunction>

</cfcomponent>