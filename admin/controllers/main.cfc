<cfcomponent accessors="true">

	<!--- Dependency Injection --->

	<!--- Controller Init --->
		<cffunction name="init" access="public" output="false" hint="Constructor">
			<cfargument name="fw" />
			<cfset variables.fw = fw />
			<cfreturn this />
		</cffunction>

	<!--- View Methods --->
		<cffunction name="default" 	access="public" returntype="void"></cffunction>

</cfcomponent>