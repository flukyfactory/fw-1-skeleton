<!---
	Filename: subsystem.cfm
	Date: Nov/2013
	Developers:
		Lester Barahona lbarahona@flukyfactory.com
		Luis Matute 	lmatute@flukyfactory.com
	Description:
		Settings per subsystem. These settings can be accessed with
		the method 'getSubsystemConfig(subsystem)'
--->

<cfset variables.framework.subsystems = {
	admin = {
		name = "Admin",
		companyName = "Admin"
	},
	public = {
		name = "Public",
		companyName = "Public"
	}
} />