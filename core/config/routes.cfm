<cfset variables.framework.routes = [
	{
		hint = "Route for the git deploy",
		'/deploy' 			= '/git/deploy'
	},
	{
		hint = "Admin Subsystem Routes",
		'/admin/*' 			= '/admin:\1'
	}
] />