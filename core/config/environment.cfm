<cfset variables.framework.environments = {

	dev = {
		<!--- FW Settings --->
			reloadApplicationOnEveryRequest = true,
			password = "true",
			trace = true,

		<!--- Project Settings --->
			mongo = {
				dsn= "",
				username = "",
				password = "",
				hosts = [ {serverName='127.0.0.1',serverPort='27017'} ]
			},
			git = {
				branch = "dev",		<!--- *1 --->
				project_name = "" 	<!--- *2 --->
			},
			splunk = {
				APIURL = "",
				token = "",
				projectID = ""
			},
			emailList = "info@flukyfactory.com",
			keyMandrillAPI = ""
	},

	prod = {
		<!--- FW Settings --->
			reloadApplicationOnEveryRequest = false,
			password = "true",
			trace = false,

		<!--- Project Settings --->
			mongo = {
				dsn= "",
				username = "",
				password = "",
				hosts = [ {serverName='127.0.0.1',serverPort='27017'} ]
			},
			git = {
				branch = "prod",	<!--- *1 --->
				project_name = "" 	<!--- *2 --->
			},
			splunk = {
				APIURL = "",
				token = "",
				projectID = ""
			},
			emailList = "info@flukyfactory.com",
			keyMandrillAPI = ""
	}
}/>


<!---
	Legend:
		1. Branch currently used for git
		2. Name Used for calling the directory when doing the 'cd /webroot/projectname'
--->