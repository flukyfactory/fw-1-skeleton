<cfset variables.framework = {
		action = 'action',
		usingSubsystems = true,
		defaultSubsystem = 'public',
		defaultSection = 'main',
		defaultItem = 'default',
		subsystemDelimiter = ':',
		home = 'public:main.default',
		error = 'error',
		reload = 'reload',
		password = 'true',
		reloadApplicationOnEveryRequest = true,
		generateSES = true,
		SESOmitIndex = true,
		applicationKey = 'core.fw1.framework',
		unhandledExtensions = 'cfc',
		unhandledPaths = '/emails/',
		baseURL = 'useRequestURI'
	}
/>